package com.org.adsales;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Testing {

	static LinkedHashMap<String, ArrayList<String>> map_rollsTypeAddids = new LinkedHashMap();
	static ArrayList<String> adIDs_Array;

	public static FileWriter fwriter;
	public static String filepath = ".\\output3.txt";
	public static String Jsonfilepath = ".\\Resources\\Json\\USAnetwork_multiRequest_Desktop.chlsj";
//	public static String Jsonfilepath = ".\\Resources\\USAnetworkBackUp.json";
//	public static String Jsonfilepath = ".\\Resources\\SYFY_FE_DesktopChrome.chlsj";
	
	public static void main(String[] args) throws IOException, ParseException {

		JSONParser parser = new JSONParser();
		Object parsedObj = parser.parse(new FileReader(Jsonfilepath));
		String adSlotsText;
		LinkedHashMap<String, Map> qu = getQueryValues(parsedObj);
		getSlotImpression(qu, "1528253896416901028");
		
		/*
		List<String> indexes = new ArrayList<String>(qu.keySet());
		LinkedHashSet<String> tops_Set = new LinkedHashSet<>();
		LinkedHashSet<String> tValue_Set = new LinkedHashSet<>();
		
		for (String d : qu.keySet()) {
//			System.out.println(d + " :" + qu.get(d));
			
			if(!qu.get(d).toString().toLowerCase().contains("caid")){
				tops_Set.add(qu.get(d).toString().split(",")[3].split("=")[1].replace("}",""));
				tValue_Set.add(qu.get(d).toString().split(",")[2].split("=")[1].replace("}",""));
			}
					
		}
		
		tops_Set.remove("NULL");
		
		for(String s: tops_Set){
			System.out.println(s);
		}
		
		for(String st: tValue_Set){
			System.out.println(st);
		}
		
		
		int counter=0;
		
		for(String tvalue:tValue_Set){
			System.out.println(tvalue);
			for(String tops:tops_Set){						
				for(int j=0;j<qu.size();j++){
					if(!qu.get(indexes.get(j)).toString().toLowerCase().contains("caid"))
					if(qu.get(indexes.get(j)).toString().split(",")[3].split("=")[1].replace("}","").equals(tops) && qu.get(indexes.get(j)).toString().contains(tvalue)){
						if(qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{","").replace("]","").toLowerCase().equals("SlotImpression".toLowerCase())){
							System.out.println("FOR tOPS "+tops+" VALUE IS: "+qu.get(indexes.get(j+1)).toString().split(",")[1].split("=")[1]);
							counter = counter+1;	
						}else if(qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{","").replace("]","").toLowerCase().equals("Slotend".toLowerCase())){
							System.out.println("FOR tOPS "+tops+" VALUE IS: "+qu.get(indexes.get(j-1)).toString().split(",")[1].split("=")[1]);
							counter = counter+1;
						}
						
						
					}
				}
				
			}
		}*/
	}
	
	
	public static String getSlotImpression(LinkedHashMap<String, Map> qu, String tvalue){

		List<String> indexes = new ArrayList<String>(qu.keySet());
		LinkedHashSet<String> tops_Set = new LinkedHashSet<>();
		LinkedHashSet<String> tValue_Set = new LinkedHashSet<>();
		
		for (String d : qu.keySet()) {
//			System.out.println(d + " :" + qu.get(d));
			
			if(!qu.get(d).toString().toLowerCase().contains("caid")){
				tops_Set.add(qu.get(d).toString().split(",")[3].split("=")[1].replace("}",""));
//				tValue_Set.add(qu.get(d).toString().split(",")[2].split("=")[1].replace("}",""));
			}
					
		}
		
		tops_Set.remove("NULL");
	/*	
		for(String s: tops_Set){
			System.out.println(s);
		}
		
		for(String st: tValue_Set){
			System.out.println(st);
		}*/
		
		
		int counter=0;
		
		/*for(String tvalue:tValue_Set){*/
			System.out.println(tvalue);
			for(String tops:tops_Set){						
				for(int j=0;j<qu.size();j++){
					if(!qu.get(indexes.get(j)).toString().toLowerCase().contains("caid"))
					if(qu.get(indexes.get(j)).toString().split(",")[3].split("=")[1].replace("}","").equals(tops) && qu.get(indexes.get(j)).toString().contains(tvalue)){
						if(qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{","").replace("]","").toLowerCase().equals("SlotImpression".toLowerCase())&& qu.get(indexes.get(j)).toString().contains(tvalue)){
							System.out.println("FOR tOPS "+tops+" VALUE IS: "+qu.get(indexes.get(j+1)).toString().split(",")[1].split("=")[1]);
							counter = counter+1;	
						}/*else if(qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{","").replace("]","").toLowerCase().equals("Slotend".toLowerCase())){
							System.out.println("FOR tOPS "+tops+" VALUE IS: "+qu.get(indexes.get(j-1)).toString().split(",")[1].split("=")[1]);
							counter = counter+1;
						}*/
						
						
					}
				}
				
			}
		/*}*/
			
			return "";
	}
	
	
	public static String stringCheck(String data) {

		data = data.replace("%20", " ");
		data = data.replace("%21", "!");
		data = data.replace("%22", "\"");
		data = data.replace("%23", "#");
		data = data.replace("%24", "$");
		data = data.replace("%25", "%");
		data = data.replace("%26", "&");
		data = data.replace("%27", "\'");
		data = data.replace("%28", "(");
		data = data.replace("%29", ")");
		data = data.replace("%2A", "*");
		data = data.replace("%2B", "+");
		data = data.replace("%2C", ",");
		data = data.replace("%2D", "-");
		data = data.replace("%2E", ".");
		data = data.replace("%2F", "/");
		data = data.replace("%30", "0");
		data = data.replace("%31", "1");
		data = data.replace("%32", "2");
		data = data.replace("%33", "3");
		data = data.replace("%34", "4");
		data = data.replace("%35", "5");
		data = data.replace("%36", "6");
		data = data.replace("%37", "7");
		data = data.replace("%38", "8");
		data = data.replace("%39", "9");
		data = data.replace("%3A", ":");
		data = data.replace("%3B", ";");
		data = data.replace("%3C", "<");
		data = data.replace("%3D", "=");
		data = data.replace("%3E", ">");
		data = data.replace("%3F", "?");
		data = data.replace("%40", "@");
		data = data.replace("%41", "A");
		data = data.replace("%42", "B");
		data = data.replace("%43", "C");
		data = data.replace("%44", "D");
		data = data.replace("%45", "E");
		data = data.replace("%46", "F");
		data = data.replace("%47", "G");
		data = data.replace("%48", "H");
		data = data.replace("%49", "I");
		data = data.replace("%4A", "J");
		data = data.replace("%4B", "K");
		data = data.replace("%4C", "L");
		data = data.replace("%4D", "M");
		data = data.replace("%4E", "N");
		data = data.replace("%4F", "O");
		data = data.replace("%50", "P");
		data = data.replace("%51", "Q");
		data = data.replace("%52", "R");
		data = data.replace("%53", "S");
		data = data.replace("%54", "T");
		data = data.replace("%55", "U");
		data = data.replace("%56", "V");
		data = data.replace("%57", "W");
		data = data.replace("%58", "X");
		data = data.replace("%59", "Y");
		data = data.replace("%5A", "Z");
		data = data.replace("%5B", "[");
		data = data.replace("%5C", "\\");
		data = data.replace("%5D", "]");
		data = data.replace("%5E", "^");
		data = data.replace("%5F", "_");
		data = data.replace("%60", "`");
		data = data.replace("%61", "a");
		data = data.replace("%62", "b");
		data = data.replace("%63", "c");
		data = data.replace("%64", "d");
		data = data.replace("%65", "e");
		data = data.replace("%66", "f");
		data = data.replace("%67", "g");
		data = data.replace("%68", "h");
		data = data.replace("%69", "i");
		data = data.replace("%6A", "j");
		data = data.replace("%6B", "k");
		data = data.replace("%6C", "l");
		data = data.replace("%6D", "m");
		data = data.replace("%6E", "n");
		data = data.replace("%6F", "o");
		data = data.replace("%70", "p");
		data = data.replace("%71", "q");
		data = data.replace("%72", "r");
		data = data.replace("%73", "s");
		data = data.replace("%74", "t");
		data = data.replace("%75", "u");
		data = data.replace("%76", "v");
		data = data.replace("%77", "w");
		data = data.replace("%78", "x");
		data = data.replace("%79", "y");
		data = data.replace("%7A", "z");
		data = data.replace("%7B", "{");
		data = data.replace("%7C", "|");
		data = data.replace("%7D", "}");
		data = data.replace("%7E", "~");
		data = data.replace("%80", "`");
		data = data.replace("+", " ");
		return data;
	}
	
	
	
	static LinkedHashMap<String, Map> getQueryValues(Object parser123) {
		ArrayList<String> queryArray = new ArrayList<String>();
		LinkedHashMap<String, Map> queryMap = new LinkedHashMap<String, Map>();
		HashMap<String, String> queryValues;

		JSONArray compleateJSON_array = (JSONArray) parser123;
		
		
		for (Object t : compleateJSON_array) {
			JSONObject obj_forPath = (JSONObject) t;

			if (obj_forPath.get("path") != null && !obj_forPath.get("path").toString().contains("g")) {
				queryArray.add(obj_forPath.get("query").toString());
			}else if(obj_forPath.get("path") != null && obj_forPath.get("path").toString().contains("g")){
//				System.out.println(obj_forPath.get("path"));
				queryArray.add(stringCheck(obj_forPath.get("query").toString()));
			}
		}
		
		//to get cn ,adid , tvalue and tpos values
		int counter = 0;
		String query;
		for (String str : queryArray) {
			query = "query";
			queryValues = new HashMap<String, String>();
			counter = counter + 1;
			query = query + counter;
			str = stringCheck(str);

			if (str.contains("&cn=")) {
				String tValue;
				try {

					tValue = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

				} catch (Exception e) {
					tValue = "NULL";
				}
				if (tValue.equals("NULL")) {
					queryValues.put("tValue", "NULL");
				} else {
					queryValues.put("tValue", tValue.split("=")[1]);
				}
				String cnValue;
				try {
					cnValue = str.substring(str.indexOf("&cn="), str.indexOf("&", str.indexOf("&cn=") + 1));
				} catch (Exception e) {
					cnValue = "NULL";
				}
				if (cnValue.equals("NULL")) {
					queryValues.put("cnValue", "NULL");
				} else {
					queryValues.put("cnValue", cnValue.split("=")[1]);
				}
				String tPosValue;
				try {
					tPosValue = str.substring(str.indexOf("&tpos="), str.indexOf("&", str.indexOf("&tpos=") + 1));
				} catch (Exception e) {
					tPosValue = "NULL";
				}
				if (tPosValue.equals("NULL")) {
					queryValues.put("tPosValue", "NULL");
				} else {
					queryValues.put("tPosValue", tPosValue.split("=")[1]);
				}
				String adidValue;
				try {
					adidValue = str.substring(str.indexOf("&adid="), str.indexOf("&", str.indexOf("&adid=") + 1));
				} catch (Exception e) {
					adidValue = "NULL";
				}
				if (adidValue.equals("NULL")) {
					queryValues.put("adidValue", "NULL");
				} else {
					queryValues.put("adidValue", adidValue.split("=")[1]);
				} 
				if (!queryValues.get("cnValue").contains("videoView")) {
					if (!queryValues.get("cnValue").contains("concreteEvent")) {
						queryMap.put(query, queryValues);
					}
				}
			}else{
				
				String caidValue;
				try {
					caidValue = str.substring(str.indexOf("&caid="), str.indexOf("&", str.indexOf("&caid=") + 1));
				} catch (Exception e) {
					caidValue = "NULL";
				}
				if (caidValue.equals("NULL")) {
					queryValues.put("caidValue", "NULL");
				} else {
					queryValues.put("caidValue", caidValue.split("=")[1]);
				}
				
				String csidValue;
				try {
					csidValue = str.substring(str.indexOf("&csid="), str.indexOf("&", str.indexOf("&csid=") + 1));
				} catch (Exception e) {
					csidValue = "NULL";
				}
				if (csidValue.equals("NULL")) {
					queryValues.put("csidValue", "NULL");
				} else {
					queryValues.put("csidValue", csidValue.split("=")[1]);
				}
				
				String tValueForRequest;
				try {
					tValueForRequest = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

				} catch (Exception e) {
					tValueForRequest = "NULL";
				}
				if (tValueForRequest.equals("NULL")) {
					queryValues.put("tValue", "NULL");
				} else {
					queryValues.put("tValue", tValueForRequest.split("=")[1]);
				}
				
				queryMap.put(query, queryValues);
			}
		

		}
		return queryMap;
	}

	}
