package com.org.adsales;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ReadXMLFile {

	public static LinkedHashMap<String, ArrayList<String>> rollsTypeAddids_xml(String path) {
		LinkedHashMap<String, ArrayList<String>> map_rollsTypeAddids = new LinkedHashMap();
		ArrayList<String> adIDs_Array;

		try {
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			int Standard_Pre = 0;
			int Post_Roll_Promo = 0;
			String rollType;

			File inputFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.print("Root element: ");
			System.out.println(doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("temporalAdSlot");
			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				// System.out.println("\nCurrent Element :");

				// System.out.println(nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					// System.out.println(eElement.getAttribute("adUnit"));
					rollType = eElement.getAttribute("adUnit");
					if (rollType.equalsIgnoreCase("midroll")) {
						midcounter = midcounter + 1;
						addName = "midroll_" + midcounter;
					} else if (rollType.equalsIgnoreCase("preroll")) {
						precounter = precounter + 1;
						addName = "preroll_" + precounter;
					} else if (rollType.equalsIgnoreCase("postroll")) {
						postcounter = postcounter + 1;
						addName = "postroll_" + postcounter;
					} else if (rollType.equalsIgnoreCase("Standard Pre")) {
						Standard_Pre = Standard_Pre + 1;
						addName = "Standard_Pre" + Standard_Pre;
					} else if (rollType.equalsIgnoreCase("Post_Roll_Promo")) {
						Post_Roll_Promo = Post_Roll_Promo + 1;
						addName = "Post_Roll_Promo" + Post_Roll_Promo;
					}
					// NodeList selectedAdsList =
					// eElement.getElementsByTagName("selectedAds");
					NodeList selectedAdsList = eElement.getChildNodes();
					// System.out.println(selectedAdsList.getLength());
					adIDs_Array = new ArrayList<>();
					for (int count = 0; count < selectedAdsList.getLength(); count++) {
						Node node_selectedAds = selectedAdsList.item(count);
						if (node_selectedAds.getNodeType() == node_selectedAds.ELEMENT_NODE) {
							Element ele_selectedAds = (Element) node_selectedAds;
							// NodeList adReferencesList =
							// ele_selectedAds.getElementsByTagName("adReference");
							NodeList adReferencesList = ele_selectedAds.getChildNodes();
							// System.out.println(adReferencesList.getLength());

							for (int adRef = 0; adRef < adReferencesList.getLength(); adRef++) {
								Node node_adRef = adReferencesList.item(adRef);
								if (node_adRef.getNodeType() == node_adRef.ELEMENT_NODE) {
									Element ele_adRef = (Element) node_adRef;
									// System.out.println(ele_adRef.getAttribute("adId"));
									if (!ele_adRef.getAttribute("adId").isEmpty())
										adIDs_Array.add(ele_adRef.getAttribute("adId"));
								}
							}
						}
						map_rollsTypeAddids.put(addName, adIDs_Array);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map_rollsTypeAddids;
	}

	public static void main(String argv[]) {
		try {

			LinkedHashMap<String, ArrayList<String>> map_rollsTypeAddids = rollsTypeAddids_xml("./Beacons.txt");

			for (String a : map_rollsTypeAddids.keySet()) {
				System.out.println(a + " is : " + map_rollsTypeAddids.get(a));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
