package com.org.adsales;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReadXMLFile2 {

	static LinkedHashMap<String, ArrayList<String>>   = new LinkedHashMap();
	static ArrayList<String> adIDs_Array;

	public static FileWriter fwriter;
	public static String filepath = "./Beacons.txt";
    //XML resp - Testdata
	public static String Jsonfilepath = "./Resources/XML/MSNBC_DesktopChromeFE_Latest.chlsj";

	
	

	public static void main(String[] args) throws IOException {
		try {
//			trails_Errors(filepath);
			
			
			JSONParser parser = new JSONParser();
			Object parsedObj = parser.parse(new FileReader(Jsonfilepath));
			String adSlotsText;
			LinkedHashMap<String, Map> qu = getQueryValues(parsedObj);
			
			/*for(String qqqqq:qu.keySet()){
				System.out.println(qqqqq+"is :"+qu.get(qqqqq));
			}*/
			
			int addReqCounter = 0;
			for(String a : qu.keySet()){
				if(qu.get(a).toString().contains("caidValue")){
					addReqCounter=addReqCounter+1;				
				}								
			}
			
			for(int i=1;i<=addReqCounter;i++){
				
				String csidValue = getCSID(parsedObj, i);
				String tValue=null;
								
				String newString = get_Body_String(parsedObj,i);
				
				
				
				
				if(newString!=null){
					
					fileWriter(newString);
					
					
					tValue = tValues_xml(filepath).toArray()[0].toString();
					
				/*	for(String a:tValues_xml(filepath)){
						System.out.println("Tvalues:::" +a);
					}*/
					
//					tValue = getTVALUE(parser, newString,i);					
					
//					LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids = getRollTypes(parser, newString);
					
					LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids = rollsTypeAddids_xml(filepath);
					
				/*	
					for (String keys : returnMap_rollsTypeAddids.keySet()) {
						  System.out.println("ArryList for key " + keys + " is : " +returnMap_rollsTypeAddids.get(keys)); 
					  }*/
					  
//					  LinkedHashMap<String, String> tposValues = exp_timePositonValues(parser, newString);
					 				  
					  HashMap<?, ?> MapFinal = splitAdIds_withRolls(returnMap_rollsTypeAddids, parsedObj,tValue);
					/*  for (Object check : MapFinal.keySet()) {
						  System.out.println(check.toString().split("__")[1] + ":" +MapFinal.get(check)); 
					  }	*/			
			
//					 LinkedHashMap<String, HashMap<String, ArrayList<String>>> M = mMap(tposValues,MapFinal);
					 HashMap<?, ?> tempTest = finalValidationMap(MapFinal);
					 
					 
					ArrayList<String> tempArrayForGrouping =  getTempArray(tempTest);
					LinkedHashMap<String, ArrayList<String>> adSolts_Grouping = get_adSolts_Grouping(tempArrayForGrouping,tempTest);
						
	
						System.out.println("");					
						System.out.println("|***************************** Video Asset "+i+" - transaction id: "+tValue+" ***************************************************************|");
						//System.out.println("Video Asset "+i+" - transaction id: "+tValue);
						System.out.println("");
	//					System.out.println("|***************************** Video Asset "+i+" - Custom Id     : "+csidValue+" ***************************************************************|");
						System.out.println("Custom Id: "+csidValue);
						System.out.println("");
						
						
						HashMap<String, Integer> psCounter = new HashMap<>();
						LinkedHashMap<String,HashMap<String, Integer>> paFailCounter = new LinkedHashMap<>();
						int co=0;
				
					for (Object gr : adSolts_Grouping.keySet()) {
						co = co + 1;
						adSlotsText = gr.toString().substring(0, 1).toUpperCase() + gr.toString().substring(1) + "_" + co;
						psCounter = new HashMap<>();
						for (int R = 0; R < adSolts_Grouping.get(gr).size(); R++) {
							String str = adSolts_Grouping.get(gr).get(R);
							if (str.toLowerCase().contains("pass")) {
								if (str.substring(str.length() - 4).toLowerCase().equals("pass")) {
									if (psCounter.containsKey("Pass")) {
										psCounter.put("Pass", psCounter.get("Pass") + 1);
									} else {
										psCounter.put("Pass", 1);
									}
								}
							} else {
								if (str.substring(str.toLowerCase().indexOf("fail"), str.toLowerCase().indexOf("fail") + 4)
										.toLowerCase().equals("fail")) {
									if (psCounter.containsKey("Fail")) {
										psCounter.put("Pass", psCounter.get("Fail") + 1);
									} else {
										psCounter.put("Fail", 1);
									}
								}
							}
						}
						if(!psCounter.containsKey("Fail")){
							psCounter.put("Fail", 0);
						}
						if(!psCounter.containsKey("Pass")){
							psCounter.put("Pass", 0);
						}
						paFailCounter.put(adSlotsText, psCounter);
					}	
						
					if(!adSolts_Grouping.isEmpty())	{											
						int cou=0;
						for (Object gr : adSolts_Grouping.keySet()) {
							cou=cou+1;
							//System.out.println("For AdSlot ::: " + gr + ":");
							adSlotsText=gr.toString().substring(0, 1).toUpperCase() + gr.toString().substring(1);
							System.out.print( adSlotsText + ":");
							System.out.println(paFailCounter.get(adSlotsText+"_"+cou));
							System.out.println("---------");
							for (int R = 0; R < adSolts_Grouping.get(gr).size(); R++) {
								System.out.println(adSolts_Grouping.get(gr).get(R));
							}
							System.out.println("");
						}	
						System.out.println("|=================================================================================================================================================|");
						System.out.println("");
					}else{
//						System.out.println("");					
//						System.out.println("|***************************** Video Asset "+i+" - transaction id: "+tValue+" ***************************************************************|");
//						System.out.println("");
						System.out.println("Custom Id: "+csidValue);
						System.out.println("");
						System.out.println("AdBeacon values are NOT available");
						System.out.println("");
						System.out.println("|=================================================================================================================================================|");
						System.out.println("");
					}
				}else{			
					System.out.println("Fail as AdBeacon values are not available");
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//===============================================================xml Methods==========================================================================================
	public static LinkedHashMap<String, ArrayList<String>> rollsTypeAddids_xml(String path) {
		LinkedHashMap<String, ArrayList<String>> map_rollsTypeAddids = new LinkedHashMap();
		ArrayList<String> adIDs_Array;

		try {
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			int Standard_Pre = 0;
			int Post_Roll_Promo = 0;
			String rollType;

			File inputFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
/*			System.out.print("Root element: ");
			System.out.println(doc.getDocumentElement().getNodeName());*/
			NodeList nList = doc.getElementsByTagName("temporalAdSlot");
//			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				// System.out.println("\nCurrent Element :");

				// System.out.println(nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					// System.out.println(eElement.getAttribute("adUnit"));
					rollType = eElement.getAttribute("adUnit");
					if (rollType.equalsIgnoreCase("midroll")) {
						midcounter = midcounter + 1;
						addName = "midroll_" + midcounter;
					} else if (rollType.equalsIgnoreCase("preroll")) {
						precounter = precounter + 1;
						addName = "preroll_" + precounter;
					} else if (rollType.equalsIgnoreCase("postroll")) {
						postcounter = postcounter + 1;
						addName = "postroll_" + postcounter;
					} else if (rollType.equalsIgnoreCase("Standard Pre")) {
						Standard_Pre = Standard_Pre + 1;
						addName = "Standard_Pre" + Standard_Pre;
					} else if (rollType.equalsIgnoreCase("Post_Roll_Promo")) {
						Post_Roll_Promo = Post_Roll_Promo + 1;
						addName = "Post_Roll_Promo" + Post_Roll_Promo;
					}
					// NodeList selectedAdsList =
					// eElement.getElementsByTagName("selectedAds");
					NodeList selectedAdsList = eElement.getChildNodes();
					// System.out.println(selectedAdsList.getLength());
					adIDs_Array = new ArrayList<>();
					for (int count = 0; count < selectedAdsList.getLength(); count++) {
						Node node_selectedAds = selectedAdsList.item(count);
						if (node_selectedAds.getNodeType() == node_selectedAds.ELEMENT_NODE) {
							Element ele_selectedAds = (Element) node_selectedAds;
							// NodeList adReferencesList =
							// ele_selectedAds.getElementsByTagName("adReference");
							NodeList adReferencesList = ele_selectedAds.getChildNodes();
							// System.out.println(adReferencesList.getLength());

							for (int adRef = 0; adRef < adReferencesList.getLength(); adRef++) {
								Node node_adRef = adReferencesList.item(adRef);
								if (node_adRef.getNodeType() == node_adRef.ELEMENT_NODE) {
									Element ele_adRef = (Element) node_adRef;
									// System.out.println(ele_adRef.getAttribute("adId"));
									if (!ele_adRef.getAttribute("adId").isEmpty())
										adIDs_Array.add(ele_adRef.getAttribute("adId"));
								}
							}
						}
						map_rollsTypeAddids.put(addName, adIDs_Array);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map_rollsTypeAddids;
	}

	
	
	
	
	public static LinkedHashSet<String> tValues_xml(String path) {
		LinkedHashSet<String> tValues_Set = new LinkedHashSet<>();

		try {			
			String tValue = null;

			File inputFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("temporalAdSlot");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					NodeList selectedAdsList = eElement.getChildNodes();
					
					for (int count = 0; count < selectedAdsList.getLength(); count++) {
						Node node_selectedAds = selectedAdsList.item(count);
						if (node_selectedAds.getNodeType() == node_selectedAds.ELEMENT_NODE) {
							Element ele_selectedAds = (Element) node_selectedAds;
							NodeList adReferencesList = ele_selectedAds.getChildNodes();
							for (int adRef = 0; adRef < adReferencesList.getLength(); adRef++) {
								Node node_adRef = adReferencesList.item(adRef);

								if (node_adRef.getNodeType() == node_adRef.ELEMENT_NODE) {
									Element ele_adRef = (Element) node_adRef;
									NodeList eventClassBack_List = ele_adRef.getChildNodes();
									for (int eventClassBack = 0; eventClassBack < eventClassBack_List.getLength(); eventClassBack++) {
										Node node_evenCBS = eventClassBack_List.item(eventClassBack);
										Element ele_evenCBS = (Element) node_evenCBS;
										NodeList ecb_List =	ele_evenCBS.getChildNodes();
										for (int ecb = 0; ecb< ecb_List.getLength(); ecb++) {
											Node node_ecb = ecb_List.item(ecb);
											Element ele_ecb = (Element) node_ecb;
											String type = ele_ecb.getAttribute("url");
											if(type.contains("&t=")){
											tValue = type.substring(type.indexOf("&t="), type.indexOf("&", type.indexOf("&t=") + 1));
											}
//											System.out.println(tValue);
											tValues_Set.add(tValue.split("=")[1].trim());
										}
									}	
								}
							}
						}					
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tValues_Set;
	}

	
	
	public static void trails_Errors(String path){

		try {			
			String tValue = null;

			File inputFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("eventCallback");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println(eElement.getAttribute("url"));
				}
			}
		}catch(Exception e){
			
		}
	}
	
	//====================================================================================================================================================================
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> getQueryValuesWithSlotIm(LinkedHashMap<String, Map> qu){

		LinkedHashMap<String, String> tempqu = new LinkedHashMap<>();
		LinkedHashMap<String, LinkedHashMap<String, String>> tempqu1 = new LinkedHashMap<>();
		
		for(String a:qu.keySet()){
			String adIDb=null;
//			System.out.println(a+" is :"+qu.get(a));
			boolean Flag =false;
			if(qu.get(a).toString().split(",")[0].split("=")[1].equalsIgnoreCase("slotImpression")||qu.get(a).toString().split(",")[0].split("=")[1].equalsIgnoreCase("slotEnd")){
				String tValue = qu.get(a).toString().split(",")[2].split("=")[1];
				String tpos = qu.get(a).toString().split(",")[3].split("=")[1];
				for(String b:qu.keySet()){
					if (!qu.get(b).toString().contains("caidValue")) {
						String tValueb = qu.get(b).toString().split(",")[2].split("=")[1];
						String tposb = qu.get(b).toString().split(",")[3].split("=")[1];
						if (qu.get(b).toString().split(",")[0].split("=")[1].equalsIgnoreCase("defaultImpression")) {
							if (tpos.equals(tposb) && tValue.equals(tValueb)) {
								adIDb = qu.get(b).toString().split(",")[1].split("=")[1];
								Flag = true;
								break;
							}
						}
					}									
				}			
			}
			if (Flag) {
				tempqu.put(a, qu.get(a).toString().replace("NULL", adIDb));
			} else {
				tempqu.put(a, qu.get(a).toString());
			} 
		}
		
		
		LinkedHashMap<String, String> maptotest = new LinkedHashMap<>();
		
		for(String r:tempqu.keySet()){
//			System.out.println("r :" +r+" is"+ tempqu.get(r));
			maptotest = new LinkedHashMap<>();
			for(int i=0;i<tempqu.get(r).split(",").length;i++){
				
				maptotest.put(tempqu.get(r).split(",")[i].split("=")[0].replace("{", "").replace("}", "").trim(), tempqu.get(r).split(",")[i].split("=")[1].replace("{", "").replace("}", "").trim());
			}
			
			tempqu1.put(r, maptotest);
			
		}
		
		return tempqu1;
		
	}

	
	public static LinkedHashMap<String, ArrayList<String>> get_adSolts_Grouping(ArrayList<String> tempArrayForGrouping,HashMap<?, ?> tempTest){
		LinkedHashSet<String> adSlotsPreRoll_Set = new LinkedHashSet<String>();
		LinkedHashSet<String> adSlotsMidRoll_Set = new LinkedHashSet<String>();
		LinkedHashSet<String> adSlots_Set = new LinkedHashSet<String>();

		ArrayList<String> adSoltsList = new ArrayList<String>();
		LinkedHashMap<String, ArrayList<String>> adSolts_Grouping = new LinkedHashMap<String, ArrayList<String>>();

		for (Object check : tempTest.keySet()) {
			String toADDinSet = check.toString().split("_")[0] + "_" + check.toString().split("_")[1];
			if (toADDinSet.toLowerCase().contains("preroll")) {
				adSlotsPreRoll_Set.add(toADDinSet);
			} else if (toADDinSet.toLowerCase().contains("midroll")) {
			    adSlotsMidRoll_Set.add(toADDinSet);
			}
		}

		for (String pre_set : adSlotsPreRoll_Set) {
			adSlots_Set.add(pre_set);
		}

		for (String mid_set : adSlotsMidRoll_Set) {
			adSlots_Set.add(mid_set);
		}

		for (String s : adSlots_Set) {
			adSoltsList = new ArrayList<String>();
			for (String tempgrpArr : tempArrayForGrouping) {
				if (tempgrpArr.toString().contains(s)) {
					adSoltsList.add(tempgrpArr);
				}
			}
			adSolts_Grouping.put(s, adSoltsList);
		}
		return adSolts_Grouping;
	}
	
	public static ArrayList<String> getTempArray(HashMap<?, ?> tempTest){	
		String expe = "defaultImpression,firstQuartile,midPoint,thirdQuartile,complete,adEnd";
		ArrayList<String> tempArrayForPreollGrouping = new ArrayList<String>();
		ArrayList<String> tempArrayForMidrollGrouping = new ArrayList<String>();
		ArrayList<String> tempArrayForGrouping = new ArrayList<String>();

		for (Object c : tempTest.keySet()) {
			if (tempTest.get(c).toString().equals(expe)) {
				if (c.toString().toLowerCase().contains("Preroll".toLowerCase())) {
					tempArrayForPreollGrouping.add("Order of Beacons fired for AdId " + c + " is as expected i.e: "+ tempTest.get(c) + ": PASS");
				} else if (c.toString().toLowerCase().contains("midroll".toLowerCase())) {
					tempArrayForMidrollGrouping.add("Order of Beacons fired for AdId " + c+ " is as expected i.e: " + tempTest.get(c) + ": PASS");
				}
			} else {

				if (c.toString().toLowerCase().contains("Preroll".toLowerCase())) {
					tempArrayForPreollGrouping.add("Order of Beacons fired for AdId " + c+ " is not as expected i.e: " + tempTest.get(c) + ": FAIL " +getFailReasons(expe,tempTest.get(c).toString()));
				} else if (c.toString().toLowerCase().contains("midroll".toLowerCase())) {
					tempArrayForMidrollGrouping.add("Order of Beacons fired for AdId " + c+ " is not as expected i.e: " + tempTest.get(c) + ": FAIL "+getFailReasons(expe,tempTest.get(c).toString()));
				}
			}
		}							

		
		for (String preRolls : tempArrayForPreollGrouping) {
			tempArrayForGrouping.add(preRolls);
		}

		for (String midRolls : tempArrayForMidrollGrouping) {
			tempArrayForGrouping.add(midRolls);
		}
		
		return tempArrayForGrouping;
	}
	
	
	public static String getCSID(Object obj,int valToBrk) {

		String csidValue_ReturnValue = "";
		String csidValue = null;
		JSONArray jarray = (JSONArray) obj;
		int counter = 0;
		for (Object o : jarray) {
			JSONObject obj1 = (JSONObject) o;
			JSONObject obj2 = (JSONObject) obj1.get("response");
			
			if (obj1.get("path") != null) {
				if (obj1.get("path").toString().equalsIgnoreCase("/ad/g/1")) {
					counter = counter + 1;
					String str_ForCSID = obj1.get("query").toString();
					str_ForCSID = stringCheck(str_ForCSID);
					try {
						csidValue = str_ForCSID.substring(str_ForCSID.indexOf("&csid="),
								str_ForCSID.indexOf("&", str_ForCSID.indexOf("&csid=") + 1));
						csidValue_ReturnValue = csidValue.split("=")[1];
						if(valToBrk==counter){
							break;	
						}
						 
					} catch (Exception e) {
						csidValue = "NULL";
					}

				}
			}

		}
		return csidValue_ReturnValue;
	}
	
	public static String getCSID_xml(Object obj,int valToBrk) {

		String csidValue_ReturnValue = "";
		String csidValue = null;
		JSONArray jarray = (JSONArray) obj;
		int counter = 0;
		for (Object o : jarray) {
			JSONObject obj1 = (JSONObject) o;
			JSONObject obj2 = (JSONObject) obj1.get("response");
			
			if (obj1.get("path") != null) {
				if (obj1.get("path").toString().equalsIgnoreCase("/ad/p/1")||obj1.get("path").toString().equalsIgnoreCase("/ad/g/1")) {/*
					counter = counter + 1;
					String str_ForCSID = obj1.get("query").toString();
					str_ForCSID = stringCheck(str_ForCSID);
					try {
						csidValue = str_ForCSID.substring(str_ForCSID.indexOf("&csid="),
								str_ForCSID.indexOf("&", str_ForCSID.indexOf("&csid=") + 1));
						csidValue_ReturnValue = csidValue.split("=")[1];
						if(valToBrk==counter){
							break;	
						}
						 
					} catch (Exception e) {
						csidValue = "NULL";
					}

				*/
					
				}
			}

		}
		return csidValue_ReturnValue;
	}



	public static LinkedHashMap<String, HashMap<String, ArrayList<String>>> mMap( LinkedHashMap<String, String> tposValues,HashMap<?, ?> MapFinal){
	  LinkedHashSet<String> tposMatchingRolls = new LinkedHashSet<String>();				  
	  LinkedHashMap<String, HashMap<String, ArrayList<String>>> M = new LinkedHashMap<String, HashMap<String,ArrayList<String>>>();
	  
	  for(String tpos : tposValues.keySet()){
		  for (Object check : MapFinal.keySet()) {		  
			  if(check.toString().contains(tpos)){
				  if(MapFinal.get(check).toString().split(",")[0].split("=")[1].equalsIgnoreCase("defaultImpression")){
					  if(MapFinal.get(check).toString().split(",")[3].split("=")[1].contains(tposValues.get(tpos).toString())){
						  tposMatchingRolls.add(check.toString().split("__")[1]);
					  }
				  }								  							  
			  }
		  }					  
	  }
	  
	  for(String abc :tposMatchingRolls){
		  for (Object check : MapFinal.keySet()) {
			  if(check.toString().contains(abc)){
				  M.put(check.toString(), (HashMap<String, ArrayList<String>>) MapFinal.get(check));
			  }
		  }
	  }
	return M;
}

	public static HashMap<?, ?> finalValidationMap(HashMap<?, ?> MapFinal) {
		LinkedHashMap<String, String> tempTest = new LinkedHashMap<String, String>();
		for (Object check : MapFinal.keySet()) {
			if (tempTest.containsKey(check.toString().split("__")[1])) {
				if(!tempTest.get(check.toString().split("__")[1]).contains(MapFinal.get(check).toString().split(",")[0].split("=")[1])){
					tempTest.put(check.toString().split("__")[1], tempTest.get(check.toString().split("__")[1]) + ","
							+ MapFinal.get(check).toString().split(",")[0].split("=")[1]);
				}
				
			} else {
				tempTest.put(check.toString().split("__")[1],
						MapFinal.get(check).toString().split(",")[0].split("=")[1]);
			}
		}
		return tempTest;
	}

	public static HashMap<?, ?> splitAdIds_withRolls(LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids,
			Object parsedObj,String tVal) {

		Map tempMap = new LinkedHashMap<String, String>();
		ArrayList<Map> finalArray = new ArrayList<Map>();
		LinkedHashMap<String, Map> MapFinal = new LinkedHashMap<String, Map>();
		LinkedHashMap<String, String> mapforFinalArray = new LinkedHashMap<String, String>();
		ArrayList<String> tempArray = new ArrayList<String>();
		
		LinkedHashMap<String, Map> qu = getQueryValues(parsedObj);
//		LinkedHashMap<String, LinkedHashMap<String, String>> qu =getQueryValuesWithSlotIm(qu0);

		for (String keys : returnMap_rollsTypeAddids.keySet()) {
			tempArray = returnMap_rollsTypeAddids.get(keys);
			String str = keys;
			for (String a : tempArray) {
				str = keys;
				str = str + "_" + a;
				mapforFinalArray.put(str, a);
			}
		}

		for (String ks : mapforFinalArray.keySet()) {
			// System.out.println("Add Id for " + ks + " is : " +
			// mapforFinalArray.get(ks));
			for (String s : qu.keySet()) {
				// System.out.println(s+": "+qu.get(s));
				if (!qu.get(s).toString().contains("caidValue")) {
					if (qu.get(s).get("adidValue").equals(mapforFinalArray.get(ks)) && qu.get(s).get("tValue").equals(tVal)) {
						tempMap = qu.get(s);
						MapFinal.put(s + "__" + ks, tempMap);
					}					
				}
			}
		}

		return MapFinal;
	}

	public static String get_Body_String(Object obj,int i) {
		String newString = null;
		try {
			JSONArray jarray = (JSONArray) obj;
			int counter = 0;
			for (Object o : jarray) {
				JSONObject obj1 = (JSONObject) o;
				JSONObject obj2 = (JSONObject) obj1.get("response");
				
				if (obj1.get("path").toString().contains("p")) {
					JSONObject obj3 = null;
					try {
						obj3 = (JSONObject) obj2.get("body");
					} catch (Exception e) {
						return null;						
					}
					if ((obj3.get("text") != null) && (!(obj3.get("text")).toString().isEmpty())) {
						
//						String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
						newString=/*str+*/obj3.get("text").toString();
						/*if((obj3.get("text").toString().contains("_videoAsset"))){
							newString ="["+obj3.get("text").toString().substring("tv.freewheel.SDK._instanceQueue['Context_2']._videoAsset.requestComplete(".length());
						}else{
							newString ="["+obj3.get("text").toString().substring("tv.freewheel.SDK._instanceQueue['Context_1'].requestComplete(".length());
						}*/
						
						/*if(obj3.get("text").toString().contains("tv.freewheel.SDK._instanceQueue['Context_1']")){												
							newString = obj3.get("text").toString().replace("tv.freewheel.SDK._instanceQueue['Context_1'].requestComplete(", "[");
						}else{
							newString = obj3.get("text").toString().replace("tv.freewheel.SDK._instanceQueue['Context_2'].requestComplete(", "[");
						}*/
						int len = newString.length();
						
						if(len<100){
							return null;
						}
//						newString = newString.substring(0, len - 2) + "]";
						counter = counter + 1;
						if(/*newString.toString().contains("")*/counter==i){
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to get the String for AdBeacons");			
		}
		return newString;
	}

	public static void fileWriter(String towrite) throws IOException {
		fwriter = new FileWriter(new File(filepath));
		fwriter.write(towrite);
		fwriter.flush();
		fwriter.close();
	}
	
	static LinkedHashMap<String, Map> getQueryValues(Object parser123, String tVal) {
	ArrayList<String> queryArray = new ArrayList<String>();
	LinkedHashMap<String, Map> queryMap = new LinkedHashMap<String, Map>();
	HashMap<String, String> queryValues;

	JSONArray compleateJSON_array = (JSONArray) parser123;
	
	
	for (Object t : compleateJSON_array) {
		JSONObject obj_forPath = (JSONObject) t;

		if (obj_forPath.get("path") != null && !obj_forPath.get("path").toString().contains("g")) {
			if (obj_forPath.get("query").toString().contains(tVal)) {
				queryArray.add(obj_forPath.get("query").toString());
			}
		}else if(obj_forPath.get("path") != null && obj_forPath.get("path").toString().contains("g")){
	
			queryArray.add(stringCheck(obj_forPath.get("query").toString()));
		}
	}

	
	//to get cn ,adid , tvalue and tpos values
	int counter = 0;
	String query;
	for (String str : queryArray) {
		query = "query";
		queryValues = new HashMap<String, String>();
		counter = counter + 1;
		query = query + counter;
		str = stringCheck(str);

		if (str.contains("&cn=")) {
			String tValue;
			try {

				tValue = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));
			} catch (Exception e) {
				tValue = "NULL";
			}
			if (tValue.equals("NULL")) {
				queryValues.put("tValue", "NULL");
			} else {
				queryValues.put("tValue", tValue.split("=")[1]);
			}
			String cnValue;
			try {
				cnValue = str.substring(str.indexOf("&cn="), str.indexOf("&", str.indexOf("&cn=") + 1));
			} catch (Exception e) {
				cnValue = "NULL";
			}
			if (cnValue.equals("NULL")) {
				queryValues.put("cnValue", "NULL");
			} else {
				queryValues.put("cnValue", cnValue.split("=")[1]);
			}
			String tPosValue;
			try {
				tPosValue = str.substring(str.indexOf("&tpos="), str.indexOf("&", str.indexOf("&tpos=") + 1));
			} catch (Exception e) {
				tPosValue = "NULL";
			}
			if (tPosValue.equals("NULL")) {
				queryValues.put("tPosValue", "NULL");
			} else {
				queryValues.put("tPosValue", tPosValue.split("=")[1]);
			}
			String adidValue;
			try {
				adidValue = str.substring(str.indexOf("&adid="), str.indexOf("&", str.indexOf("&adid=") + 1));
			} catch (Exception e) {
				adidValue = "NULL";
			}
			if (adidValue.equals("NULL")) {
				queryValues.put("adidValue", "NULL");
			} else {
				queryValues.put("adidValue", adidValue.split("=")[1]);
			} 
			if (!queryValues.get("cnValue").contains("videoView")) {
				if (!queryValues.get("cnValue").contains("concreteEvent")) {
					queryMap.put(query, queryValues);
				}
			}
		}else{
			String caidValue;
			try {
				caidValue = str.substring(str.indexOf("&caid="), str.indexOf("&", str.indexOf("&caid=") + 1));
			} catch (Exception e) {
				caidValue = "NULL";
			}
			if (caidValue.equals("NULL")) {
				queryValues.put("caidValue", "NULL");
			} else {
				queryValues.put("caidValue", caidValue.split("=")[1]);
			}
			
			String csidValue;
			try {
				csidValue = str.substring(str.indexOf("&csid="), str.indexOf("&", str.indexOf("&csid=") + 1));
			} catch (Exception e) {
				csidValue = "NULL";
			}
			if (csidValue.equals("NULL")) {
				queryValues.put("csidValue", "NULL");
			} else {
				queryValues.put("csidValue", csidValue.split("=")[1]);
			}
			
			String tValueForRequest;
			try {
				tValueForRequest = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

			} catch (Exception e) {
				tValueForRequest = "NULL";
			}
			if (tValueForRequest.equals("NULL")) {
				queryValues.put("tValue", "NULL");
			} else {
				queryValues.put("tValue", tValueForRequest.split("=")[1]);
			}			
			queryMap.put(query, queryValues);
		}
	}
	return queryMap;
}
		
	static LinkedHashMap<String, Map> getQueryValues(Object parser123) {
		ArrayList<String> queryArray = new ArrayList<String>();
		LinkedHashMap<String, Map> queryMap = new LinkedHashMap<String, Map>();
		HashMap<String, String> queryValues;

		JSONArray compleateJSON_array = (JSONArray) parser123;
		
		
		for (Object t : compleateJSON_array) {
			JSONObject obj_forPath = (JSONObject) t;

			if (obj_forPath.get("path") != null && !obj_forPath.get("path").toString().contains("p")) {
				queryArray.add(obj_forPath.get("query").toString());
//				System.out.println(obj_forPath.get("query").toString());
			}else if(obj_forPath.get("path") != null && obj_forPath.get("path").toString().contains("p")||obj_forPath.get("path") != null && obj_forPath.get("path").toString().contains("g")){
		
				queryArray.add(stringCheck(obj_forPath.get("query").toString()));
				System.out.println(stringCheck(obj_forPath.get("query").toString()));
			}
		}
		
		//to get cn ,adid , tvalue and tpos values
		int counter = 0;
		String query;
		for (String str : queryArray) {
			query = "query";
			queryValues = new HashMap<String, String>();
			counter = counter + 1;
			query = query + counter;
			str = stringCheck(str);

			if (str.contains("&cn=")) {
				String tValue;
				try {

					tValue = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

				} catch (Exception e) {
					tValue = "NULL";
				}
				if (tValue.equals("NULL")) {
					queryValues.put("tValue", "NULL");
				} else {
					queryValues.put("tValue", tValue.split("=")[1]);
				}
				String cnValue;
				try {
					cnValue = str.substring(str.indexOf("&cn="), str.indexOf("&", str.indexOf("&cn=") + 1));
				} catch (Exception e) {
					cnValue = "NULL";
				}
				if (cnValue.equals("NULL")) {
					queryValues.put("cnValue", "NULL");
				} else {
					queryValues.put("cnValue", cnValue.split("=")[1]);
				}
				String tPosValue;
				try {
					tPosValue = str.substring(str.indexOf("&tpos="), str.indexOf("&", str.indexOf("&tpos=") + 1));
				} catch (Exception e) {
					tPosValue = "NULL";
				}
				if (tPosValue.equals("NULL")) {
					queryValues.put("tPosValue", "NULL");
				} else {
					queryValues.put("tPosValue", tPosValue.split("=")[1]);
				}
				String adidValue;
				try {
					adidValue = str.substring(str.indexOf("&adid="), str.indexOf("&", str.indexOf("&adid=") + 1));
				} catch (Exception e) {
					adidValue = "NULL";
				}
				if (adidValue.equals("NULL")) {
					queryValues.put("adidValue", "NULL");
				} else {
					queryValues.put("adidValue", adidValue.split("=")[1]);
				} 
				if (!queryValues.get("cnValue").contains("videoView")) {
					if (!queryValues.get("cnValue").contains("concreteEvent")) {
						queryMap.put(query, queryValues);
					}
				}
			}else{
				
				String caidValue;
				try {
					caidValue = str.substring(str.indexOf("&caid="), str.indexOf("&", str.indexOf("&caid=") + 1));
				} catch (Exception e) {
					caidValue = "NULL";
				}
				if (caidValue.equals("NULL")) {
					queryValues.put("caidValue", "NULL");
				} else {
					queryValues.put("caidValue", caidValue.split("=")[1]);
				}
				
				String csidValue;
				try {
					csidValue = str.substring(str.indexOf("&csid="), str.indexOf("&", str.indexOf("&csid=") + 1));
				} catch (Exception e) {
					csidValue = "NULL";
				}
				if (csidValue.equals("NULL")) {
					queryValues.put("csidValue", "NULL");
				} else {
					queryValues.put("csidValue", csidValue.split("=")[1]);
				}
				
				String tValueForRequest;
				try {
					tValueForRequest = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

				} catch (Exception e) {
					tValueForRequest = "NULL";
				}
				if (tValueForRequest.equals("NULL")) {
					queryValues.put("tValue", "NULL");
				} else {
					queryValues.put("tValue", tValueForRequest.split("=")[1]);
				}
				
				queryMap.put(query, queryValues);
			}
		

		}
		return queryMap;
	}

	static String getTVALUE(JSONParser parser, String newString,int val)
			throws ParseException {
		String tValue = null;
		try {
			Object obj_BodyText = parser.parse(newString);
			JSONArray text_array = (JSONArray) obj_BodyText;
			int Counter =0;
			
			top: for (Object t : text_array) {
				JSONObject ads_Obj = (JSONObject) t;
				
				JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
				JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
				for (Object ads : adSlotsArray) {
					JSONObject eventCalls = (JSONObject) ads;
					JSONArray forURL = (JSONArray) eventCalls.get("eventCallbacks");
					for (Object u : forURL) {
						JSONObject L = (JSONObject) u;
						
						try {

							tValue = L.get("url").toString().substring(L.get("url").toString().indexOf("&t="), L.get("url").toString().indexOf("&", L.get("url").toString().indexOf("&t=") + 1));
							 if(Counter == val){
								 return tValue.split("=")[1];
							 }
						} catch (Exception e) {
							tValue = "NULL";
						}

					}
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to parse the string: " +newString);
			e.printStackTrace();
		}
			return tValue.split("=")[1];
	}
		
	static LinkedHashMap<String, ArrayList<String>> getRollTypes(JSONParser parser, String newString)
			throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			// System.out.println(adSlotsArray.size());
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				String rollType = eventCalls.get("timePositionClass").toString();
				// System.out.println(rollType);

				if (rollType.equalsIgnoreCase("midroll")) {
					midcounter = midcounter + 1;
					addName = "midroll_" + midcounter;
				} else if (rollType.equalsIgnoreCase("preroll")) {
					precounter = precounter + 1;
					addName = "preroll_" + precounter;
				} else if (rollType.equalsIgnoreCase("postroll")) {
					precounter = precounter + 1;
					addName = "postroll_" + postcounter;
				}

				// JSONObject eventCalls = (JSONObject) ads;
				adIDs_Array = new ArrayList();
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");
				for (Object u : forURL) {

					JSONObject L = (JSONObject) u;
					// System.out.println(L.get("adId"));
					adIDs_Array.add(L.get("adId").toString());
				}
				map_rollsTypeAddids.put(addName, adIDs_Array);
			}
		}
		return map_rollsTypeAddids;
	}
		
	static LinkedHashMap<String, String> exp_timePositonValues(JSONParser parser, String newString)
			throws ParseException {
		
		LinkedHashMap<String, String> map_rolls_tPOS = new LinkedHashMap<String, String>();
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			// System.out.println(adSlotsArray.size());
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				String tPos = eventCalls.get("timePosition").toString();
				String rollType = eventCalls.get("timePositionClass").toString();
				

				if (rollType.equalsIgnoreCase("midroll")) {
					midcounter = midcounter + 1;
					addName = "midroll_" + midcounter;
				} else if (rollType.equalsIgnoreCase("preroll")) {
					precounter = precounter + 1;
					addName = "preroll_" + precounter;
				} else if (rollType.equalsIgnoreCase("postroll")) {
					precounter = precounter + 1;
					addName = "postroll_" + postcounter;
				}

				map_rolls_tPOS.put(addName, tPos);
			}
			
		}
		return map_rolls_tPOS;
	}

	static void getsAddIDs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			String str_rollName = null;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");

				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					System.out.println(L.get("adId"));

				}

			}
		}
	}

	static void getsiteSection_eventCallbacksURLs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("eventCallbacks");
				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					System.out.println(L.get("url"));
				}
			}
		}
	}

	static void getsiteSection_selectedAdsURLs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");
				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					// System.out.println(L.get("url"));
					JSONArray eventCallsbacks = (JSONArray) L.get("eventCallbacks");
					for (Object ec : eventCallsbacks) {
						JSONObject urlSelectedAds = (JSONObject) ec;
						System.out.println(urlSelectedAds.get("url"));
					}
				}

			}
		}
	}

	static void getAds(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONArray adArrays = (JSONArray) ((JSONObject) ads_Obj.get("ads")).get("ads");
			for (Object ads : adArrays) {
				JSONObject adID = (JSONObject) ads;
				System.out.println(adID.get("adId"));
			}
		}
	}

	public static String stringCheck(String data) {

		data = data.replace("%20", " ");
		data = data.replace("%21", "!");
		data = data.replace("%22", "\"");
		data = data.replace("%23", "#");
		data = data.replace("%24", "$");
		data = data.replace("%25", "%");
		data = data.replace("%26", "&");
		data = data.replace("%27", "\'");
		data = data.replace("%28", "(");
		data = data.replace("%29", ")");
		data = data.replace("%2A", "*");
		data = data.replace("%2B", "+");
		data = data.replace("%2C", ",");
		data = data.replace("%2D", "-");
		data = data.replace("%2E", ".");
		data = data.replace("%2F", "/");
		data = data.replace("%30", "0");
		data = data.replace("%31", "1");
		data = data.replace("%32", "2");
		data = data.replace("%33", "3");
		data = data.replace("%34", "4");
		data = data.replace("%35", "5");
		data = data.replace("%36", "6");
		data = data.replace("%37", "7");
		data = data.replace("%38", "8");
		data = data.replace("%39", "9");
		data = data.replace("%3A", ":");
		data = data.replace("%3B", ";");
		data = data.replace("%3C", "<");
		data = data.replace("%3D", "=");
		data = data.replace("%3E", ">");
		data = data.replace("%3F", "?");
		data = data.replace("%40", "@");
		data = data.replace("%41", "A");
		data = data.replace("%42", "B");
		data = data.replace("%43", "C");
		data = data.replace("%44", "D");
		data = data.replace("%45", "E");
		data = data.replace("%46", "F");
		data = data.replace("%47", "G");
		data = data.replace("%48", "H");
		data = data.replace("%49", "I");
		data = data.replace("%4A", "J");
		data = data.replace("%4B", "K");
		data = data.replace("%4C", "L");
		data = data.replace("%4D", "M");
		data = data.replace("%4E", "N");
		data = data.replace("%4F", "O");
		data = data.replace("%50", "P");
		data = data.replace("%51", "Q");
		data = data.replace("%52", "R");
		data = data.replace("%53", "S");
		data = data.replace("%54", "T");
		data = data.replace("%55", "U");
		data = data.replace("%56", "V");
		data = data.replace("%57", "W");
		data = data.replace("%58", "X");
		data = data.replace("%59", "Y");
		data = data.replace("%5A", "Z");
		data = data.replace("%5B", "[");
		data = data.replace("%5C", "\\");
		data = data.replace("%5D", "]");
		data = data.replace("%5E", "^");
		data = data.replace("%5F", "_");
		data = data.replace("%60", "`");
		data = data.replace("%61", "a");
		data = data.replace("%62", "b");
		data = data.replace("%63", "c");
		data = data.replace("%64", "d");
		data = data.replace("%65", "e");
		data = data.replace("%66", "f");
		data = data.replace("%67", "g");
		data = data.replace("%68", "h");
		data = data.replace("%69", "i");
		data = data.replace("%6A", "j");
		data = data.replace("%6B", "k");
		data = data.replace("%6C", "l");
		data = data.replace("%6D", "m");
		data = data.replace("%6E", "n");
		data = data.replace("%6F", "o");
		data = data.replace("%70", "p");
		data = data.replace("%71", "q");
		data = data.replace("%72", "r");
		data = data.replace("%73", "s");
		data = data.replace("%74", "t");
		data = data.replace("%75", "u");
		data = data.replace("%76", "v");
		data = data.replace("%77", "w");
		data = data.replace("%78", "x");
		data = data.replace("%79", "y");
		data = data.replace("%7A", "z");
		data = data.replace("%7B", "{");
		data = data.replace("%7C", "|");
		data = data.replace("%7D", "}");
		data = data.replace("%7E", "~");
		data = data.replace("%80", "`");
		data = data.replace("+", " ");
		return data;
	}
	
	public static String getFailReasons(String expe_value,String act_value){
		List<String> missing_List = new ArrayList<String>();
		String return_String=null;
		if(expe_value.length()==act_value.length()){
			List<String> exp_Items = Arrays.asList(expe_value.split("\\s*,\\s*"));
			List<String> act_Items =  Arrays.asList(act_value.split("\\s*,\\s*"));
			for(int i=0;i<exp_Items.size();i++){					
				if(!exp_Items.get(i).equals(act_Items.get(i))){
					missing_List.add(act_Items.get(i));
				}	
			}
			
			return_String = " - Fired AdBeacons order is wrong: "+missing_List;
		}else if(expe_value.length()<act_value.length()){
			List<String> exp_Items = Arrays.asList(expe_value.split("\\s*,\\s*"));
			List<String> act_Items =  Arrays.asList(act_value.split("\\s*,\\s*"));			
			act_Items.stream().filter( x -> !exp_Items.contains(x) ).forEach(x -> missing_List.add(x));
			return_String = " - Unexpected value got fired: "+missing_List;
		}else if(expe_value.length()>act_value.length()){
			List<String> exp_Items = Arrays.asList(expe_value.split("\\s*,\\s*"));
			List<String> act_Items =  Arrays.asList(act_value.split("\\s*,\\s*"));
			exp_Items.stream().filter( x -> !act_Items.contains(x) ).forEach(x -> missing_List.add(x));				
			return_String = " - AdBeacons are missing: "+missing_List;
		}
		
		
		return return_String;
		
	}

}
