package com.org.adsales;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonAdBeacons {

	static LinkedHashMap<String, ArrayList<String>> map_rollsTypeAddids = new LinkedHashMap();
	static ArrayList<String> adIDs_Array;

	public static FileWriter fwriter;
	public static String filepath = ".\\output.txt";
//	 public static String Jsonfilepath = ".\\Resources\\USAnetworkBackUp.json";
	// public static String Jsonfilepath = ".\\Resources\\CNBC_DesktopChrome.chlsj";
	public static String Jsonfilepath = ".\\Resources\\USAnetwork_multiRequest_Desktop.chlsj";
	// public static String Jsonfilepath = ".\\Resources\\Bravo_FE_DesktopChrome.chlsj";

	public static void main(String[] args) throws IOException {
		try {
			JSONParser parser = new JSONParser();
			Object parsedObj = parser.parse(new FileReader(Jsonfilepath));

			System.out.println("CSID :" + getCSID(parsedObj));

			LinkedHashMap<String, Map> qu = getQueryValues(parsedObj);
			
			/*for (String s : qu.keySet()) {
				System.out.println(s + ": " + qu.get(s));
			}*/

			String newString = get_Body_String(parsedObj);
			fileWriter(newString);
			
			LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids = getRollTypes(parser, newString);
			
			System.out.println("returnMap_rollsTypeAddids");
			  for (String keys : returnMap_rollsTypeAddids.keySet()) {
				  System.out.println("ArryList for key " + keys + " is : " +returnMap_rollsTypeAddids.get(keys)); 
			  }
			 

			HashMap<?, ?> MapFinal = splitAdIds_withRolls(returnMap_rollsTypeAddids, parsedObj);
			
			  for (Object check : MapFinal.keySet()) {
				  System.out.println(check.toString().split("__")[1] + ":" +MapFinal.get(check)); 
			  }
			 

			HashMap<?, ?> tempTest = finalValidationMap(MapFinal);
			
			String expe = "defaultImpression,firstQuartile,midPoint,thirdQuartile,complete,adEnd";
			ArrayList<String> tempArrayForPreollGrouping = new ArrayList<String>();
			ArrayList<String> tempArrayForMidrollGrouping = new ArrayList<String>();
			for (Object c : tempTest.keySet()) {
				if (tempTest.get(c).toString().equals(expe)) {
					if (c.toString().toLowerCase().contains("Preroll".toLowerCase())) {
						tempArrayForPreollGrouping.add("Order of Beacons fired for Ad id " + c + " is as expected i.e: "+ tempTest.get(c) + " Pass");
					} else if (c.toString().toLowerCase().contains("midroll".toLowerCase())) {
						tempArrayForMidrollGrouping.add("Order of Beacons fired for Ad id " + c+ " is as expected i.e: " + tempTest.get(c) + " Pass");
					}
				} else {

					if (c.toString().toLowerCase().contains("Preroll".toLowerCase())) {
						tempArrayForPreollGrouping.add("Order of Beacons fired for Ad id " + c+ " is not as expected i.e: " + tempTest.get(c) + " Fail");
					} else if (c.toString().toLowerCase().contains("midroll".toLowerCase())) {
						tempArrayForMidrollGrouping.add("Order of Beacons fired for Ad id " + c+ " is not as expected i.e: " + tempTest.get(c) + " Fail");
					}
				}
			}

			ArrayList<String> tempArrayForGrouping = new ArrayList<String>();

			for (String preRolls : tempArrayForPreollGrouping) {
				tempArrayForGrouping.add(preRolls);
			}

			for (String midRolls : tempArrayForMidrollGrouping) {
				tempArrayForGrouping.add(midRolls);
			}

			LinkedHashSet<String> adSlotsPreRoll_Set = new LinkedHashSet<String>();
			LinkedHashSet<String> adSlotsMidRoll_Set = new LinkedHashSet<String>();
			LinkedHashSet<String> adSlots_Set = new LinkedHashSet<String>();

			ArrayList<String> adSoltsList = new ArrayList<String>();
			LinkedHashMap<String, ArrayList<String>> adSolts_Grouping = new LinkedHashMap<String, ArrayList<String>>();

			for (Object check : tempTest.keySet()) {
				String toADDinSet = check.toString().split("_")[0] + "_" + check.toString().split("_")[1];
				if (toADDinSet.toLowerCase().contains("preroll")) {
					adSlotsPreRoll_Set.add(toADDinSet);
				} else if (toADDinSet.toLowerCase().contains("midroll")) {
					adSlotsMidRoll_Set.add(toADDinSet);
				}
			}

			for (String pre_set : adSlotsPreRoll_Set) {
				adSlots_Set.add(pre_set);
			}

			for (String mid_set : adSlotsMidRoll_Set) {
				adSlots_Set.add(mid_set);
			}

			for (String s : adSlots_Set) {
				adSoltsList = new ArrayList<String>();
				for (String tempgrpArr : tempArrayForGrouping) {
					if (tempgrpArr.toString().contains(s)) {
						adSoltsList.add(tempgrpArr);
					}
				}
				adSolts_Grouping.put(s, adSoltsList);
			}

			System.out.println("List of Beacons Fired:");
			System.out.println("");

			for (Object gr : adSolts_Grouping.keySet()) {
				System.out.println("For adSlot " + gr + ":");

				for (int i = 0; i < adSolts_Grouping.get(gr).size(); i++) {
					System.out.println(adSolts_Grouping.get(gr).get(i));
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getCSID(Object obj) {

		String csidValue_ReturnValue = "";
		String csidValue = null;
		JSONArray jarray = (JSONArray) obj;
		int counter = 0;
		for (Object o : jarray) {
			JSONObject obj1 = (JSONObject) o;
			JSONObject obj2 = (JSONObject) obj1.get("response");
			counter = counter + 1;
			if (obj1.get("path") != null) {
				if (obj1.get("path").toString().equalsIgnoreCase("/ad/g/1")) {
					String str_ForCSID = obj1.get("query").toString();
					str_ForCSID = stringCheck(str_ForCSID);
					try {
						csidValue = str_ForCSID.substring(str_ForCSID.indexOf("&csid="),
								str_ForCSID.indexOf("&", str_ForCSID.indexOf("&csid=") + 1));
						// System.out.println(csidValue);
//						csidValue_ReturnValue = csidValue.split("=")[1] + "#" + csidValue_ReturnValue;
						csidValue_ReturnValue = csidValue.split("=")[1];
						 break;
					} catch (Exception e) {
						csidValue = "NULL";
					}

				}
			}

		}
		return csidValue_ReturnValue;
	}

	public static HashMap<?, ?> finalValidationMap(HashMap<?, ?> MapFinal) {
		LinkedHashMap<String, String> tempTest = new LinkedHashMap<String, String>();
		for (Object check : MapFinal.keySet()) {
			if (tempTest.containsKey(check.toString().split("__")[1])) {
				tempTest.put(check.toString().split("__")[1], tempTest.get(check.toString().split("__")[1]) + ","
						+ MapFinal.get(check).toString().split(",")[0].split("=")[1]);
			} else {
				tempTest.put(check.toString().split("__")[1],
						MapFinal.get(check).toString().split(",")[0].split("=")[1]);
			}
		}
		return tempTest;
	}

	public static HashMap<?, ?> splitAdIds_withRolls(LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids,
			Object parsedObj) {

		Map tempMap = new LinkedHashMap<String, String>();
		ArrayList<Map> finalArray = new ArrayList<Map>();
		LinkedHashMap<String, Map> MapFinal = new LinkedHashMap<String, Map>();
		LinkedHashMap<String, String> mapforFinalArray = new LinkedHashMap<String, String>();
		ArrayList<String> tempArray = new ArrayList<String>();
		LinkedHashMap<String, Map> qu = getQueryValues(parsedObj);

		for (String keys : returnMap_rollsTypeAddids.keySet()) {
			tempArray = returnMap_rollsTypeAddids.get(keys);
			String str = keys;
			for (String a : tempArray) {
				str = keys;
				str = str + "_" + a;
				mapforFinalArray.put(str, a);
			}
		}

		for (String ks : mapforFinalArray.keySet()) {
			// System.out.println("Add Id for " + ks + " is : " +
			// mapforFinalArray.get(ks));
			for (String s : qu.keySet()) {
				// System.out.println(s+": "+qu.get(s));

				if (qu.get(s).get("adidValue").equals(mapforFinalArray.get(ks))) {
					tempMap = qu.get(s);
					MapFinal.put(s + "__" + ks, tempMap);
				}
			}
		}

		return MapFinal;
	}

	public static String get_Body_String(Object obj) {

		String newString = null;
		JSONArray jarray = (JSONArray) obj;
		int counter = 0;
		for (Object o : jarray) {
			JSONObject obj1 = (JSONObject) o;
			JSONObject obj2 = (JSONObject) obj1.get("response");
			counter = counter + 1;
			if (obj1.get("path") != null) {
				JSONObject obj3 = (JSONObject) obj2.get("body");
				if ((obj3.get("text") != null) && (!(obj3.get("text")).toString().isEmpty())) {
					newString = obj3.get("text").toString()
							.replace("tv.freewheel.SDK._instanceQueue['Context_1'].requestComplete(", "[");
					int len = newString.length();
					newString = newString.substring(0, len - 2) + "]";
				}
			}
		}
		return newString;
	}

	public static void fileWriter(String towrite) throws IOException {
		fwriter = new FileWriter(new File(filepath));
		fwriter.write(towrite);
		fwriter.flush();
		fwriter.close();
	}

	static LinkedHashMap<String, Map> getQueryValues(Object parser123) {
		ArrayList<String> queryArray = new ArrayList<String>();
		LinkedHashMap<String, Map> queryMap = new LinkedHashMap<String, Map>();
		HashMap<String, String> queryValues;

		JSONArray compleateJSON_array = (JSONArray) parser123;
		for (Object t : compleateJSON_array) {
			JSONObject obj_forPath = (JSONObject) t;

			if (obj_forPath.get("path") != null && !obj_forPath.get("path").toString().contains("g")) {
				queryArray.add(obj_forPath.get("query").toString());
			}
		}

		int counter = 0;
		String query;
		for (String str : queryArray) {
			query = "query";
			queryValues = new HashMap<String, String>();
			counter = counter + 1;
			query = query + counter;
			str = stringCheck(str);

			String tValue;
			try {

				tValue = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

			} catch (Exception e) {
				tValue = "NULL";
			}

			if (tValue.equals("NULL")) {
				queryValues.put("tValue", "NULL");
			} else {
				queryValues.put("tValue", tValue.split("=")[1]);
			}

			String cnValue;
			try {
				cnValue = str.substring(str.indexOf("&cn="), str.indexOf("&", str.indexOf("&cn=") + 1));
			} catch (Exception e) {
				cnValue = "NULL";
			}

			if (cnValue.equals("NULL")) {
				queryValues.put("cnValue", "NULL");
			} else {
				queryValues.put("cnValue", cnValue.split("=")[1]);
			}

			String tPosValue;
			try {
				tPosValue = str.substring(str.indexOf("&tpos="), str.indexOf("&", str.indexOf("&tpos=") + 1));
			} catch (Exception e) {
				tPosValue = "NULL";
			}

			if (tPosValue.equals("NULL")) {
				queryValues.put("tPosValue", "NULL");
			} else {
				queryValues.put("tPosValue", tPosValue.split("=")[1]);
			}

			String adidValue;
			try {
				adidValue = str.substring(str.indexOf("&adid="), str.indexOf("&", str.indexOf("&adid=") + 1));
			} catch (Exception e) {
				adidValue = "NULL";
			}

			if (adidValue.equals("NULL")) {
				queryValues.put("adidValue", "NULL");
			} else {
				queryValues.put("adidValue", adidValue.split("=")[1]);
			}

			String caidValue;
			try {
				caidValue = str.substring(str.indexOf("&caid="), str.indexOf("&", str.indexOf("&caid=") + 1));
			} catch (Exception e) {
				caidValue = "NULL";
			}

			if (caidValue.equals("NULL")) {
				queryValues.put("caidValue", "NULL");
			} else {
				queryValues.put("caidValue", caidValue.split("=")[1]);
			}

			if (!queryValues.get("cnValue").contains("videoView")) {
				if (!queryValues.get("cnValue").contains("concreteEvent")) {
					queryMap.put(query, queryValues);
				}
			}

		}
		return queryMap;
	}

	static LinkedHashMap<String, ArrayList<String>> getRollTypes(JSONParser parser, String newString)
			throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			// System.out.println(adSlotsArray.size());
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				String rollType = eventCalls.get("timePositionClass").toString();
				// System.out.println(rollType);

				if (rollType.equalsIgnoreCase("midroll")) {
					midcounter = midcounter + 1;
					addName = "midroll_" + midcounter;
				} else if (rollType.equalsIgnoreCase("preroll")) {
					precounter = precounter + 1;
					addName = "preroll_" + precounter;
				} else if (rollType.equalsIgnoreCase("postroll")) {
					precounter = precounter + 1;
					addName = "postroll_" + postcounter;
				}

				// JSONObject eventCalls = (JSONObject) ads;
				adIDs_Array = new ArrayList();
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");
				for (Object u : forURL) {

					JSONObject L = (JSONObject) u;
					// System.out.println(L.get("adId"));
					adIDs_Array.add(L.get("adId").toString());
				}
				map_rollsTypeAddids.put(addName, adIDs_Array);
			}
		}
		return map_rollsTypeAddids;
	}

	static void getsAddIDs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			String str_rollName = null;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");

				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					System.out.println(L.get("adId"));

				}

			}
		}
	}

	static void getsiteSection_eventCallbacksURLs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("eventCallbacks");
				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					System.out.println(L.get("url"));
				}
			}
		}
	}

	static void getsiteSection_selectedAdsURLs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");
				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					// System.out.println(L.get("url"));
					JSONArray eventCallsbacks = (JSONArray) L.get("eventCallbacks");
					for (Object ec : eventCallsbacks) {
						JSONObject urlSelectedAds = (JSONObject) ec;
						System.out.println(urlSelectedAds.get("url"));
					}
				}

			}
		}
	}

	static void getAds(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONArray adArrays = (JSONArray) ((JSONObject) ads_Obj.get("ads")).get("ads");
			for (Object ads : adArrays) {
				JSONObject adID = (JSONObject) ads;
				System.out.println(adID.get("adId"));
			}
		}
	}

	public static String stringCheck(String data) {

		data = data.replace("%20", " ");
		data = data.replace("%21", "!");
		data = data.replace("%22", "\"");
		data = data.replace("%23", "#");
		data = data.replace("%24", "$");
		data = data.replace("%25", "%");
		data = data.replace("%26", "&");
		data = data.replace("%27", "\'");
		data = data.replace("%28", "(");
		data = data.replace("%29", ")");
		data = data.replace("%2A", "*");
		data = data.replace("%2B", "+");
		data = data.replace("%2C", ",");
		data = data.replace("%2D", "-");
		data = data.replace("%2E", ".");
		data = data.replace("%2F", "/");
		data = data.replace("%30", "0");
		data = data.replace("%31", "1");
		data = data.replace("%32", "2");
		data = data.replace("%33", "3");
		data = data.replace("%34", "4");
		data = data.replace("%35", "5");
		data = data.replace("%36", "6");
		data = data.replace("%37", "7");
		data = data.replace("%38", "8");
		data = data.replace("%39", "9");
		data = data.replace("%3A", ":");
		data = data.replace("%3B", ";");
		data = data.replace("%3C", "<");
		data = data.replace("%3D", "=");
		data = data.replace("%3E", ">");
		data = data.replace("%3F", "?");
		data = data.replace("%40", "@");
		data = data.replace("%41", "A");
		data = data.replace("%42", "B");
		data = data.replace("%43", "C");
		data = data.replace("%44", "D");
		data = data.replace("%45", "E");
		data = data.replace("%46", "F");
		data = data.replace("%47", "G");
		data = data.replace("%48", "H");
		data = data.replace("%49", "I");
		data = data.replace("%4A", "J");
		data = data.replace("%4B", "K");
		data = data.replace("%4C", "L");
		data = data.replace("%4D", "M");
		data = data.replace("%4E", "N");
		data = data.replace("%4F", "O");
		data = data.replace("%50", "P");
		data = data.replace("%51", "Q");
		data = data.replace("%52", "R");
		data = data.replace("%53", "S");
		data = data.replace("%54", "T");
		data = data.replace("%55", "U");
		data = data.replace("%56", "V");
		data = data.replace("%57", "W");
		data = data.replace("%58", "X");
		data = data.replace("%59", "Y");
		data = data.replace("%5A", "Z");
		data = data.replace("%5B", "[");
		data = data.replace("%5C", "\\");
		data = data.replace("%5D", "]");
		data = data.replace("%5E", "^");
		data = data.replace("%5F", "_");
		data = data.replace("%60", "`");
		data = data.replace("%61", "a");
		data = data.replace("%62", "b");
		data = data.replace("%63", "c");
		data = data.replace("%64", "d");
		data = data.replace("%65", "e");
		data = data.replace("%66", "f");
		data = data.replace("%67", "g");
		data = data.replace("%68", "h");
		data = data.replace("%69", "i");
		data = data.replace("%6A", "j");
		data = data.replace("%6B", "k");
		data = data.replace("%6C", "l");
		data = data.replace("%6D", "m");
		data = data.replace("%6E", "n");
		data = data.replace("%6F", "o");
		data = data.replace("%70", "p");
		data = data.replace("%71", "q");
		data = data.replace("%72", "r");
		data = data.replace("%73", "s");
		data = data.replace("%74", "t");
		data = data.replace("%75", "u");
		data = data.replace("%76", "v");
		data = data.replace("%77", "w");
		data = data.replace("%78", "x");
		data = data.replace("%79", "y");
		data = data.replace("%7A", "z");
		data = data.replace("%7B", "{");
		data = data.replace("%7C", "|");
		data = data.replace("%7D", "}");
		data = data.replace("%7E", "~");
		data = data.replace("%80", "`");
		data = data.replace("+", " ");
		return data;
	}
}
