package com.org.adsales;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonAdBeacons_ForMultiRequest {

	static LinkedHashMap<String, ArrayList<String>> map_rollsTypeAddids = new LinkedHashMap();
	static ArrayList<String> adIDs_Array;
	static LinkedHashMap<String, LinkedHashMap<String, String>> unExpectedBeacons;
	static LinkedHashMap<String, LinkedHashMap<String, String>> addedTops;

	public static FileWriter fwriter;
	public static String filepath = ".\\output.txt";
	//JSON resp - Testdata
	public static String Jsonfilepath = ".\\Resources\\Json\\USAnetwork_multiRequest_Desktop.chlsj";
//	public static String Jsonfilepath = "./Resources/Json/USAnetwork_multiRequest_Desktop.chlsj";
//	public static String Jsonfilepath = "./Resources/Json/Bravo_FE_DesktopChrome.chlsj";
//	public static String Jsonfilepath = "./Resources/Json/CNBC_DesktopChrome.chlsj";
//	public static String Jsonfilepath = "./Resources/Json/EOnline_FE_DesktopChrome.chlsj";  // In Midroll1, one addid(23977688) is missing in Adbeacon.
//	public static String Jsonfilepath = "./Resources/Json/NBC_FE_DesktopChrome.chlsj";
//	public static String Jsonfilepath = "./Resources/Json/DAPSH-957_Chrome_Ses-1.chlsj";
//	public static String Jsonfilepath = "./Resources/Json/SYFY_FE_DesktopChrome.chlsj";
	
	//TODO TelemundoNow_FE_DesktopChrome.chlsj: 1 issue, SlotImpression is present, but in Test result it has been failed. slotEnd is only missing.
		//resolved [stopped adding slotImpression]
//	public static String Jsonfilepath = "./Resources/Json/TelemundoNow_FE_DesktopChrome.chlsj";
	
	//TODO NBCSports_PrerogetRollTypesll_DesktopChrome.chlsj  : 1 issue - AdBeacons are present for the t value 1526622711207209008, but it's not captured.
		// This need to be handled as the only beacon (for expected adid) fired is [cnValue=_e_3p-comp]. rest of beacons fired not belong to exp adid		
//	public static String Jsonfilepath = "./Resources/Json/NBCSports_Preroll_DesktopChrome.chlsj";
	
	
//	public static String Jsonfilepath = "./Resources/Json/dapsh-886_Android mbl_no_ads_june5.chlsj"; // Tested
	
	//TODO dapsh-866-iPhone-Noticias.chlsj: 2 issues: SlotImpression is present with 'tpos' value and 't' values  & getting ERROR as ArrayIndexOutOfBoundsException: 4
		//Done, now its working fine [Resolved two issues]
//	public static String Jsonfilepath = "./Resources/Json/dapsh-866-iPhone-Noticias.chlsj";////this file will help to explain all the possiblities 
	
//	TODO dapsh-886-iphone-adInventory_6June_sasi.chlsj: 2 Issues: SlotImpression and slotEnd beacons are available, but not captured. t value is 1528313335270207003. Refer this t value in AdRequest and AdBeacon for valiation.
		//as there are no adids, it will not capture any of the beacons...! If require, it need to be handled
//	public static String Jsonfilepath = "./Resources/Json/dapsh-886-iphone-adInventory_6June_sasi.chlsj";
	
	
	//TODO DAPSH-957_Chrome_Ses-1.chlsj: 1 issue - 2 AdRequests only available, but Testresult shows 3 AdRequests. t value 1528907065300711025 is repeated. 
				//Resolved and I think now the out put is as expected
		
//	public static String Jsonfilepath = "./Resources/Json/DAPSH-957_Chrome_Ses-1.chlsj";
	
	

//	public static String Jsonfilepath = "./Resources/Json/DAPSH-957_Chrome_Ses-1.chlsj";
	

	public static void main(String[] args) throws IOException {
		try {
			
			
			
			JSONParser parser = new JSONParser();
			
			//Jsonfilepath: is the path of the file of Json that is exported from json
			Object parsedObj = parser.parse(new FileReader(Jsonfilepath));
			String adSlotsText;

			
			//			getQueryValues(): This method will fetch all the query's  
			LinkedHashMap<String, Map> qu_forSlot = getQueryValues(parsedObj);
			for(String d:qu_forSlot.keySet()){
				System.out.println(d+" :"+ qu_forSlot.get(d));
			}
			
			
			LinkedHashSet<String> tValue_Set = new LinkedHashSet<>();
			for(String d:qu_forSlot.keySet()){
				if(!qu_forSlot.get(d).toString().contains("caid"))
				tValue_Set.add(qu_forSlot.get(d).toString().split(",")[2].split("=")[1].replace("}",""));
			}
			
			
			/*for(String tvalues : tValue_Set){
					System.out.println(tvalues);
			}*/
			
			
			int addReqCounter = 0;
			for(String a : qu_forSlot.keySet()){
				if(qu_forSlot.get(a).toString().contains("caidValue")){
					if(!qu_forSlot.get(a).toString().split(",")[2].split("=")[1].replace("}","").equals("NULL")){
						addReqCounter=addReqCounter+1;
					}	
				}
			}
			int adRequests=0;
			System.out.println("Total AdRequests - "+addReqCounter);
			String resourcesName=null;
			String resources_Res = ".\\Result\\Ressult_Resources\\"+resourcesName+".txt";
			
			String resultName=null;
			String Resutls_folderPath = ".\\Result\\"+resultName+".txt";
			for(int i=1;i<=addReqCounter;i++){
				
				
				unExpectedBeacons = new LinkedHashMap<>();
				adRequests=i;
				String csidValue = getCSID(parsedObj, i);
				String tValue=null;
								
				String newString = get_Body_String(parsedObj,i);
				
				if(newString!=null){
					
					fileWriter(newString);
					
					tValue = getTVALUE(parser, newString,i);					
					resources_Res = ".\\Result\\Ressult_Resources\\"+tValue+".txt";
					LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids = getRollTypes(parser, newString);
					 for (String keys : returnMap_rollsTypeAddids.keySet()) {
						  System.out.println("ArryList for key " + keys + " is : " +returnMap_rollsTypeAddids.get(keys)); 
					  }
					  
					  LinkedHashMap<String, String> tposValues = exp_timePositonValues(parser, newString);
					 				  
					  HashMap<?, ?> MapFinal = splitAdIds_withRolls(returnMap_rollsTypeAddids, parsedObj,tValue);
					  
					  
//					  System.out.println("MapFinal is as below");
					  for(Object s:MapFinal.keySet()){
						  System.out.println(s+" is a follows: "+MapFinal.get(s));
					  }
					  
					  
					  LinkedHashMap<String, String> topMap = new LinkedHashMap<>();
					 for (Object check : MapFinal.keySet()) {
//						  System.out.println(check.toString()/*.split("__")[1] */+ ":" +MapFinal.get(check)); 
						  
						  
						  if(MapFinal.get(check).toString().split(",")[0].split("=")[1].toLowerCase().equals("slotImpression".toLowerCase())){
							  topMap.put(check.toString().split("__")[1].split("_")[0]+"_"+check.toString().split("__")[1].split("_")[1] , MapFinal.get(check).toString().split(",")[3].split("=")[1].replace("}", ""));
						  }
					  }		
				/*	 for(String t: topMap.keySet()){
						 System.out.println("For value "+t+" is :"+topMap.get(t));
					 }*/
					
				
					 
			
					 LinkedHashMap<String, HashMap<String, ArrayList<String>>> M = mMap(tposValues,MapFinal);
					 HashMap<?, ?> tempTest = finalValidationMap(M);
					 
					 
					ArrayList<String> tempArrayForGrouping =  getTempArray(tempTest);
					LinkedHashMap<String, ArrayList<String>> adSolts_Grouping = get_adSolts_Grouping(tempArrayForGrouping,tempTest);
						
	
						System.out.println("");					
						System.out.println("|***************************** Video Asset "+i+" - transaction id: "+tValue+" ***************************************************************|");
						//System.out.println("Video Asset "+i+" - transaction id: "+tValue);
						System.out.println("");
						System.out.println("URL:"+getQuery(parsedObj,i));
						System.out.println("");
	//					System.out.println("|***************************** Video Asset "+i+" - Custom Id     : "+csidValue+" ***************************************************************|");
						System.out.println("Custom Id: "+csidValue);
						System.out.println("");
						
						
						HashMap<String, Integer> psCounter = new HashMap<>();
						LinkedHashMap<String,HashMap<String, Integer>> paFailCounter = new LinkedHashMap<>();
						int co=0;
				
					for (Object gr : adSolts_Grouping.keySet()) {
						co = co + 1;
						adSlotsText = gr.toString().substring(0, 1).toUpperCase() + gr.toString().substring(1) + "_" + co;
						psCounter = new HashMap<>();
						for (int R = 0; R < adSolts_Grouping.get(gr).size(); R++) {
							String str = adSolts_Grouping.get(gr).get(R);
							if (str.toLowerCase().contains("pass")) {
								if (str.substring(str.length() - 4).toLowerCase().equals("pass")) {
									if (psCounter.containsKey("Pass")) {
										psCounter.put("Pass", psCounter.get("Pass") + 1);
									} else {
										psCounter.put("Pass", 1);
									}
								}
							} else {
								if (str.substring(str.toLowerCase().indexOf("fail"), str.toLowerCase().indexOf("fail") + 4)
										.toLowerCase().equals("fail")) {
									if (psCounter.containsKey("Fail")) {
										psCounter.put("Pass", psCounter.get("Fail") + 1);
									} else {
										psCounter.put("Fail", 1);
									}
								}
							}
						}
						String adSlotsText_emp = adSlotsText.substring(0,adSlotsText.length()-2);
						if(getSlotImpression(qu_forSlot, tValue,i,tposValues.get(adSlotsText_emp.toLowerCase())).toLowerCase().contains("pass")){
							if (psCounter.containsKey("Pass")) {
								psCounter.put("Pass", psCounter.get("Pass") + 1);
							} else {
								psCounter.put("Pass", 1);
							}
							
						}else if(getSlotImpression(qu_forSlot, tValue,i,tposValues.get(adSlotsText_emp.toLowerCase())).toLowerCase().contains("fail")){
							if (psCounter.containsKey("Fail")) {
								psCounter.put("Fail", psCounter.get("Fail") + 1);
							} else {
								psCounter.put("Fail", 1);
							}
						}
						
						if(getSlotEnd(qu_forSlot, tValue,i,tposValues.get(adSlotsText_emp.toLowerCase())).toLowerCase().contains("pass")){
							if (psCounter.containsKey("Pass")) {
								psCounter.put("Pass", psCounter.get("Pass") + 1);
							} else {
								psCounter.put("Pass", 1);
							}
							
						}else if(getSlotEnd(qu_forSlot, tValue,i,tposValues.get(adSlotsText_emp.toLowerCase())).toLowerCase().contains("fail")){
							if (psCounter.containsKey("Fail")) {
								psCounter.put("Pass", psCounter.get("Fail") + 1);
							} else {
								psCounter.put("Fail", 1);
							}
						}
						
						if(!unExpectedBeacons.isEmpty()){
							for(String s:unExpectedBeacons.keySet()){
								if(unExpectedBeacons.get(s).get("tPosValue").equals(tposValues.get(adSlotsText_emp.toLowerCase()))&&unExpectedBeacons.get(s).get("tValue").equals(tValue))
//								System.out.println("Unexpected Beacons fired for:"+unExpectedBeacons.get(s).toString().split(",")[1]+" i.e, "+unExpectedBeacons.get(s).toString().split(",")[0].split("=")[1]+" : FAIL");
									if (psCounter.containsKey("Fail")) {
										psCounter.put("Pass", psCounter.get("Fail") + 1);
									} else {
										psCounter.put("Fail", 1);
									}
							}
						}
						
						if(!psCounter.containsKey("Fail")){
							psCounter.put("Fail", 0);
						}
						if(!psCounter.containsKey("Pass")){
							psCounter.put("Pass", 0);
						}
						paFailCounter.put(adSlotsText, psCounter);
					}	
				
					if(!adSolts_Grouping.isEmpty())	{

				
						int cou=0;
						for (Object gr : adSolts_Grouping.keySet()) {
							cou=cou+1;
							//System.out.println("For AdSlot ::: " + gr + ":");
							adSlotsText=gr.toString().substring(0, 1).toUpperCase() + gr.toString().substring(1);							
							System.out.print( adSlotsText + ":");
						
						
							
							System.out.println(paFailCounter.get(adSlotsText+"_"+cou));
							System.out.println("---------");
							System.out.println(getSlotImpression(qu_forSlot, tValue/*tValuetValue_Set.toArray()[i-1].toString()*/,i,tposValues.get(adSlotsText.toLowerCase())));
							for (int R = 0; R < adSolts_Grouping.get(gr).size(); R++) {
								System.out.println(adSolts_Grouping.get(gr).get(R));
							}
							
							if(!unExpectedBeacons.isEmpty()){
								for(String s:unExpectedBeacons.keySet()){
									if(unExpectedBeacons.get(s).get("tPosValue").equals(tposValues.get(adSlotsText.toLowerCase()))&&unExpectedBeacons.get(s).get("tValue").equals(tValue))
									System.out.println("Unexpected Beacons fired for:"+unExpectedBeacons.get(s).toString().split(",")[1]+" i.e, "+unExpectedBeacons.get(s).toString().split(",")[0].split("=")[1]+" : FAIL");
								}
							}	
							System.out.println(getSlotEnd(qu_forSlot, tValue/*tValuetValue_Set.toArray()[i-1].toString()*/,i,tposValues.get(adSlotsText.toLowerCase())));
							System.out.println("");
							
							/*if(!unExpectedBeacons.isEmpty()){
								for(String s:unExpectedBeacons.keySet()){
									if(unExpectedBeacons.get(s).get("tValue").equals(tValue))
									System.out.println("For "+s+" is "+"Unexpected Beacons fired :"+unExpectedBeacons.get(s));
								}
							}*/
						}	
						System.out.println("|=================================================================================================================================================|");
						System.out.println("");
					}else{
//						System.out.println("");					
//						System.out.println("|***************************** Video Asset "+i+" - transaction id: "+tValue+" ***************************************************************|");
//						System.out.println("");
//						System.out.println("Custom Id: "+csidValue);
						System.out.println(getSlotImpression(qu_forSlot, tValue/*tValuetValue_Set.toArray()[i-1].toString()*/,i));
						System.out.println("");
						System.out.println("FAIL: AdBeacon values are NOT available");
						System.out.println("");												
						if(!unExpectedBeacons.isEmpty()){
							for(String s:unExpectedBeacons.keySet()){
								if(unExpectedBeacons.get(s).get("tValue").equals(tValue))
								System.out.println("Unexpected Beacons fired for:"+unExpectedBeacons.get(s).toString().split(",")[1]+" i.e, "+unExpectedBeacons.get(s).toString().split(",")[0].split("=")[1]);
							}
						}
						System.out.println(getSlotEnd(qu_forSlot, tValue/*tValuetValue_Set.toArray()[i-1].toString()*/,i));
						System.out.println("|=================================================================================================================================================|");
						System.out.println("");
					}
				}else{	
					System.out.println("");
					System.out.println("AdRequest - "+adRequests);
					System.out.println("No AdRequset Found");
					System.out.println("=================================");
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> getQueryValuesWithSlotIm(LinkedHashMap<String, Map> qu){

		LinkedHashMap<String, String> tempqu = new LinkedHashMap<>();
		LinkedHashMap<String, LinkedHashMap<String, String>> tempqu1 = new LinkedHashMap<>();
		
		for(String a:qu.keySet()){
			String adIDb=null;
//			System.out.println(a+" is :"+qu.get(a));
			boolean Flag =false;
			if(qu.get(a).toString().split(",")[0].split("=")[1].equalsIgnoreCase("slotImpression")/*||qu.get(a).toString().split(",")[0].split("=")[1].equalsIgnoreCase("slotEnd")*/){
				String tValue = qu.get(a).toString().split(",")[2].split("=")[1];
				String tpos = qu.get(a).toString().split(",")[3].split("=")[1];
				for(String b:qu.keySet()){
					if (!qu.get(b).toString().contains("caidValue")) {
						String tValueb = qu.get(b).toString().split(",")[2].split("=")[1];
						String tposb = qu.get(b).toString().split(",")[3].split("=")[1];
						if (qu.get(b).toString().split(",")[0].split("=")[1].equalsIgnoreCase("defaultImpression")) {
							if (tpos.equals(tposb) && tValue.equals(tValueb)) {
								adIDb = qu.get(b).toString().split(",")[1].split("=")[1];
								Flag = true;
								break;
							}
						}
					}									
				}			
			}
			if (Flag) {
				tempqu.put(a, qu.get(a).toString().replace("NULL", ""));
			} else {
				tempqu.put(a, qu.get(a).toString());
			} 
		}
		
		
		LinkedHashMap<String, String> maptotest = new LinkedHashMap<>();
		
		for(String r:tempqu.keySet()){
//			System.out.println("r :" +r+" is"+ tempqu.get(r));
			maptotest = new LinkedHashMap<>();
			for(int i=0;i<tempqu.get(r).split(",").length;i++){
				String key="NULL";
				try {
					key=tempqu.get(r).split(",")[i].split("=")[0].replace("{", "").replace("}", "").trim();
				} catch (Exception e) {
					key="NULL";
				}
		
				String value="";
				try {
					value=tempqu.get(r).split(",")[i].split("=")[1].replace("{", "").replace("}", "").trim();
				} catch (Exception e) {
					value="NULL";
				}
		
				maptotest.put(key, value);
			}
			
			tempqu1.put(r, maptotest);
			
		}
		LinkedHashMap<String, LinkedHashMap<String, String>> addedTposValues = addingTposValues(tempqu1);
		
	/*	for(String mw: tempqu1.keySet()){
			System.out.println("This is"+mw+" is :"+tempqu1.get(mw));
		}
		
		for(String q: addedTposValues.keySet()){
			System.out.println("This is"+q+" is :"+addedTposValues.get(q));
		}*/
		
		/*if(true){
			return unExpectedBeacons;
		}*/
		LinkedHashMap<String, LinkedHashMap<String, String>> mapwithOutUnexpectedBeacons = new LinkedHashMap<>();
		String expe_beacons = "defaultImpression,firstQuartile,midPoint,thirdQuartile,complete,adEnd,slotImpression,slotEnd";

		for(String mwub: tempqu1.keySet()){
			if(!tempqu1.get(mwub).toString().toLowerCase().contains("caidValue".toLowerCase())){
				if(expe_beacons.toLowerCase().contains(tempqu1.get(mwub).get("cnValue").toLowerCase().trim())){
					mapwithOutUnexpectedBeacons.put(mwub, tempqu1.get(mwub));				
				}else{
					unExpectedBeacons.put(mwub, addedTposValues.get(mwub));
				}
			}				
		}
		
		return mapwithOutUnexpectedBeacons;
		
	}
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> addingTposValues(LinkedHashMap<String, LinkedHashMap<String, String>> tempqu1){
		addedTops = new LinkedHashMap<>();
		LinkedHashMap<String, String> temp_addedTops;
		String tPosValue =null;		
		for(String s:tempqu1.keySet()){
			temp_addedTops = new LinkedHashMap<>();
			if(!tempqu1.get(s).toString().toLowerCase().contains("caid")){
				
				if(tempqu1.get(s).get("cnValue").equalsIgnoreCase("slotImpression")){
					tPosValue= tempqu1.get(s).get("tPosValue");
					temp_addedTops.put("cnValue", tempqu1.get(s).get("cnValue"));
					temp_addedTops.put("adidValue", tempqu1.get(s).get("adidValue"));
					temp_addedTops.put("tValue", tempqu1.get(s).get("tValue"));
					temp_addedTops.put("tPosValue", tPosValue);					
				}else{			
					temp_addedTops.put("cnValue", tempqu1.get(s).get("cnValue"));
					temp_addedTops.put("adidValue", tempqu1.get(s).get("adidValue"));
					temp_addedTops.put("tValue", tempqu1.get(s).get("tValue"));
					temp_addedTops.put("tPosValue", tPosValue);
				}					
			}else{
				temp_addedTops.put("tValue", tempqu1.get(s).get("tValue"));
				temp_addedTops.put("csidValue", tempqu1.get(s).get("csidValue"));
				temp_addedTops.put("caidValue", tempqu1.get(s).get("caidValue"));				
			}	
			
			addedTops.put(s,temp_addedTops);
		}
		
		return addedTops;
	}
	
	public static LinkedHashMap<String, ArrayList<String>> get_adSolts_Grouping(ArrayList<String> tempArrayForGrouping,HashMap<?, ?> tempTest){
		LinkedHashSet<String> adSlotsPreRoll_Set = new LinkedHashSet<String>();
		LinkedHashSet<String> adSlotsMidRoll_Set = new LinkedHashSet<String>();
		LinkedHashSet<String> adSlots_Set = new LinkedHashSet<String>();

		ArrayList<String> adSoltsList = new ArrayList<String>();
		LinkedHashMap<String, ArrayList<String>> adSolts_Grouping = new LinkedHashMap<String, ArrayList<String>>();

		for (Object check : tempTest.keySet()) {
			String toADDinSet = check.toString().split("_")[0] + "_" + check.toString().split("_")[1];
			if (toADDinSet.toLowerCase().contains("preroll")) {
				adSlotsPreRoll_Set.add(toADDinSet);
			} else if (toADDinSet.toLowerCase().contains("midroll")) {
			    adSlotsMidRoll_Set.add(toADDinSet);
			}
		}

		for (String pre_set : adSlotsPreRoll_Set) {
			adSlots_Set.add(pre_set);
		}

		for (String mid_set : adSlotsMidRoll_Set) {
			adSlots_Set.add(mid_set);
		}

		for (String s : adSlots_Set) {
			adSoltsList = new ArrayList<String>();
			for (String tempgrpArr : tempArrayForGrouping) {
				if (tempgrpArr.toString().contains(s)) {
					adSoltsList.add(tempgrpArr);
				}
			}
			adSolts_Grouping.put(s, adSoltsList);
		}
		return adSolts_Grouping;
	}
	
	public static ArrayList<String> getTempArray(HashMap<?, ?> tempTest){	
		String expe = "defaultImpression,firstQuartile,midPoint,thirdQuartile,complete,adEnd";
		ArrayList<String> tempArrayForPreollGrouping = new ArrayList<String>();
		ArrayList<String> tempArrayForMidrollGrouping = new ArrayList<String>();
		ArrayList<String> tempArrayForGrouping = new ArrayList<String>();

		for (Object c : tempTest.keySet()) {
			if (tempTest.get(c).toString().equals(expe)) {
				if (c.toString().toLowerCase().contains("Preroll".toLowerCase())) {
					//tempArrayForPreollGrouping.add("Order of Beacons fired for AdId " + c + " is as expected i.e: "+ tempTest.get(c) + ": PASS");
					tempArrayForPreollGrouping.add("Order of Beacons fired for AdId " + c + " is as expected i.e: "+ tempTest.get(c) + ": PASS");
				} else if (c.toString().toLowerCase().contains("midroll".toLowerCase())) {
					tempArrayForMidrollGrouping.add("Order of Beacons fired for AdId " + c+ " is as expected i.e: " + tempTest.get(c) + ": PASS");
				}
			} else {

				if (c.toString().toLowerCase().contains("Preroll".toLowerCase())) {
					tempArrayForPreollGrouping.add("Order of Beacons fired for AdId " + c+ " is not as expected i.e: " + tempTest.get(c) + ": FAIL " +getFailReasons(expe,tempTest.get(c).toString()));
				} else if (c.toString().toLowerCase().contains("midroll".toLowerCase())) {
					tempArrayForMidrollGrouping.add("Order of Beacons fired for AdId " + c+ " is not as expected i.e: " + tempTest.get(c) + ": FAIL "+getFailReasons(expe,tempTest.get(c).toString()));
				}
			}
		}							

		
		for (String preRolls : tempArrayForPreollGrouping) {
			tempArrayForGrouping.add(preRolls);
		}

		for (String midRolls : tempArrayForMidrollGrouping) {
			tempArrayForGrouping.add(midRolls);
		}
		
		return tempArrayForGrouping;
	}
		
	public static String getQuery(Object obj,int valToBrk) {
		
		String str_ForQuery = null;
		JSONArray jarray = (JSONArray) obj;
		int counter = 0;
		for (Object o : jarray) {
			JSONObject obj1 = (JSONObject) o;
			JSONObject obj2 = (JSONObject) obj1.get("response");
			
			if (obj1.get("path") != null) {
				if (obj1.get("path").toString().equalsIgnoreCase("/ad/g/1")||obj1.get("path").toString().equalsIgnoreCase("/ad/p/1")) {
					counter = counter + 1;
					str_ForQuery ="https://"+obj1.get("host").toString()+obj1.get("path").toString()+obj1.get("query").toString();
					if(valToBrk==counter){
						break;	
					}
				}
			}				
		}			
		return str_ForQuery;
	}
		
	public static String getCSID(Object obj,int valToBrk) {
		

		String csidValue_ReturnValue = "";
		String csidValue = null;
		JSONArray jarray = (JSONArray) obj;
		int counter = 0;
		for (Object o : jarray) {
			JSONObject obj1 = (JSONObject) o;
			JSONObject obj2 = (JSONObject) obj1.get("response");
			
			if (obj1.get("path") != null) {
				if (obj1.get("path").toString().equalsIgnoreCase("/ad/g/1")) {
					counter = counter + 1;
					String str_ForCSID = obj1.get("query").toString();
					str_ForCSID = stringCheck(str_ForCSID);
					try {
						csidValue = str_ForCSID.substring(str_ForCSID.indexOf("&csid="),
								str_ForCSID.indexOf("&", str_ForCSID.indexOf("&csid=") + 1));
						csidValue_ReturnValue = csidValue.split("=")[1];
						if(valToBrk==counter){
							break;	
						}
						 
					} catch (Exception e) {
						csidValue = "NULL";
					}

				}
			}

		}
		return csidValue_ReturnValue;
	}

	public static LinkedHashMap<String, HashMap<String, ArrayList<String>>> mMap( LinkedHashMap<String, String> tposValues,HashMap<?, ?> MapFinal){
	  LinkedHashSet<String> tposMatchingRolls = new LinkedHashSet<String>();				  
	  LinkedHashMap<String, HashMap<String, ArrayList<String>>> M = new LinkedHashMap<String, HashMap<String,ArrayList<String>>>();
	  
	  for(String tpos : tposValues.keySet()){
		  for (Object check : MapFinal.keySet()) {		  
			  if(check.toString().contains(tpos)){
				  if(MapFinal.get(check).toString().split(",")[0].split("=")[1].equalsIgnoreCase("defaultImpression")){
					  if(MapFinal.get(check).toString().split(",")[3].split("=")[1].contains(tposValues.get(tpos).toString())){
						  tposMatchingRolls.add(check.toString().split("__")[1]);
					  }
				  }								  							  
			  }
		  }					  
	  }
	  
	  for(String abc :tposMatchingRolls){
		  for (Object check : MapFinal.keySet()) {
			  if(check.toString().contains(abc)){
				  M.put(check.toString(), (HashMap<String, ArrayList<String>>) MapFinal.get(check));
			  }
		  }
	  }
	return M;
}

	public static HashMap<?, ?> finalValidationMap(HashMap<?, ?> MapFinal) {
		LinkedHashMap<String, String> tempTest = new LinkedHashMap<String, String>();
		for (Object check : MapFinal.keySet()) {
			if (tempTest.containsKey(check.toString().split("__")[1])) {
				if(!(MapFinal.get(check).toString().split(",")[0].split("=")[1].toString().equalsIgnoreCase("slotImpression")))
				if(!tempTest.get(check.toString().split("__")[1]).contains(MapFinal.get(check).toString().split(",")[0].split("=")[1])){
					tempTest.put(check.toString().split("__")[1], tempTest.get(check.toString().split("__")[1]) + ","
							+ MapFinal.get(check).toString().split(",")[0].split("=")[1]);
				}
				
			} else {
				if(!(MapFinal.get(check).toString().split(",")[0].split("=")[1].toString().equalsIgnoreCase("slotImpression")))
				tempTest.put(check.toString().split("__")[1],
						MapFinal.get(check).toString().split(",")[0].split("=")[1]);
			}
		}
		return tempTest;
	}

	public static HashMap<?, ?> splitAdIds_withRolls(LinkedHashMap<String, ArrayList<String>> returnMap_rollsTypeAddids,
			Object parsedObj,String tVal) {

		Map tempMap = new LinkedHashMap<String, String>();
		ArrayList<Map> finalArray = new ArrayList<Map>();
		LinkedHashMap<String, Map> MapFinal = new LinkedHashMap<String, Map>();
		LinkedHashMap<String, String> mapforFinalArray = new LinkedHashMap<String, String>();
		ArrayList<String> tempArray = new ArrayList<String>();
		
		LinkedHashMap<String, Map> qu1 = getQueryValues(parsedObj);
		LinkedHashMap<String, LinkedHashMap<String, String>> qu =getQueryValuesWithSlotIm(qu1);
		/*for (String s : qu.keySet()) {
			System.out.println("Map qu "+s+" is "+qu.get(s));
		}*/
	
		for (String keys : returnMap_rollsTypeAddids.keySet()) {
			tempArray = returnMap_rollsTypeAddids.get(keys);
			String str = keys;
			for (String a : tempArray) {
				str = keys;
				str = str + "_" + a;
				mapforFinalArray.put(str, a);
			}
		}

		for (String ks : mapforFinalArray.keySet()) {
			// System.out.println("Add Id for " + ks + " is : " +
			// mapforFinalArray.get(ks));
			for (String s : qu.keySet()) {
				// System.out.println(s+": "+qu.get(s));
				if (!qu.get(s).toString().contains("caidValue")) {
					if (qu.get(s).get("adidValue").equals(mapforFinalArray.get(ks)) && qu.get(s).get("tValue").equals(tVal)) {
						tempMap = qu.get(s);
						MapFinal.put(s + "__" + ks, tempMap);
					}else if(qu.get(s).get("adidValue").equals("NULL") && qu.get(s).get("tValue").equals(tVal) && qu.get(s).get("cnValue").toLowerCase().equals("slotImpression".toLowerCase())){
						tempMap = qu.get(s);
						MapFinal.put(s + "__" + ks, tempMap);
					}
				}
			}
		}

		return MapFinal;
	}

	public static String get_Body_String(Object obj,int i) {
		String newString = null;
		try {
			JSONArray jarray = (JSONArray) obj;
			int counter = 0;
			for (Object o : jarray) {
				JSONObject obj1 = (JSONObject) o;
				JSONObject obj2 = (JSONObject) obj1.get("response");
				
				if (obj1.get("path") != null) {
					JSONObject obj3 = null;
					try {
						obj3 = (JSONObject) obj2.get("body");
					} catch (Exception e) {
						return null;						
					}
					if ((obj3.get("text") != null) && (!(obj3.get("text")).toString().isEmpty())) {
						if((obj3.get("text").toString().contains("_videoAsset"))){
							newString ="["+obj3.get("text").toString().substring("tv.freewheel.SDK._instanceQueue['Context_2']._videoAsset.requestComplete(".length());
						}else{
							newString ="["+obj3.get("text").toString().substring("tv.freewheel.SDK._instanceQueue['Context_1'].requestComplete(".length());
						}
						
						/*if(obj3.get("text").toString().contains("tv.freewheel.SDK._instanceQueue['Context_1']")){												
							newString = obj3.get("text").toString().replace("tv.freewheel.SDK._instanceQueue['Context_1'].requestComplete(", "[");
						}else{
							newString = obj3.get("text").toString().replace("tv.freewheel.SDK._instanceQueue['Context_2'].requestComplete(", "[");
						}*/
						int len = newString.length();
						
						if(len<100){
							return null;
						}
						newString = newString.substring(0, len - 2) + "]";
						counter = counter + 1;
						if(counter==i){
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to get the String for AdBeacons");			
		}
		return newString;
	}

	public static void fileWriter(String towrite) throws IOException {
		fwriter = new FileWriter(new File(filepath));
		fwriter.write(towrite);
		fwriter.flush();
		fwriter.close();
	}
	
	static LinkedHashMap<String, Map> getQueryValues(Object parser123, String tVal) {
	ArrayList<String> queryArray = new ArrayList<String>();
	LinkedHashMap<String, Map> queryMap = new LinkedHashMap<String, Map>();
	HashMap<String, String> queryValues;

	JSONArray compleateJSON_array = (JSONArray) parser123;
	
	
	for (Object t : compleateJSON_array) {
		JSONObject obj_forPath = (JSONObject) t;

		if (obj_forPath.get("path") != null && !obj_forPath.get("path").toString().contains("g")) {
			if (obj_forPath.get("query").toString().contains(tVal)) {
				queryArray.add(obj_forPath.get("query").toString());
			}
		}else if(obj_forPath.get("path") != null && obj_forPath.get("path").toString().contains("g")){
	
			queryArray.add(stringCheck(obj_forPath.get("query").toString()));
		}
	}

	
	//to get cn ,adid , tvalue and tpos values
	int counter = 0;
	String query;
	for (String str : queryArray) {
		query = "query";
		queryValues = new HashMap<String, String>();
		counter = counter + 1;
		query = query + counter;
		str = stringCheck(str);

		if (str.contains("&cn=")) {
			String tValue;
			try {

				tValue = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));
			} catch (Exception e) {
				tValue = "NULL";
			}
			if (tValue.equals("NULL")) {
				queryValues.put("tValue", "NULL");
			} else {
				queryValues.put("tValue", tValue.split("=")[1]);
			}
			String cnValue;
			try {
				cnValue = str.substring(str.indexOf("&cn="), str.indexOf("&", str.indexOf("&cn=") + 1));
			} catch (Exception e) {
				cnValue = "NULL";
			}
			if (cnValue.equals("NULL")) {
				queryValues.put("cnValue", "NULL");
			} else {
				queryValues.put("cnValue", cnValue.split("=")[1]);
			}
			String tPosValue;
			try {
				tPosValue = str.substring(str.indexOf("&tpos="), str.indexOf("&", str.indexOf("&tpos=") + 1));
			} catch (Exception e) {
				tPosValue = "NULL";
			}
			if (tPosValue.equals("NULL")) {
				queryValues.put("tPosValue", "NULL");
			} else {
				queryValues.put("tPosValue", tPosValue.split("=")[1]);
			}
			String adidValue;
			try {
				adidValue = str.substring(str.indexOf("&adid="), str.indexOf("&", str.indexOf("&adid=") + 1));
			} catch (Exception e) {
				adidValue = "NULL";
			}
			if (adidValue.equals("NULL")) {
				queryValues.put("adidValue", "NULL");
			} else {
				queryValues.put("adidValue", adidValue.split("=")[1]);
			} 
			if (!queryValues.get("cnValue").contains("videoView")) {
				if (!queryValues.get("cnValue").contains("concreteEvent")) {
					queryMap.put(query, queryValues);
				}
			}
		}else{
			String caidValue;
			try {
				caidValue = str.substring(str.indexOf("&caid="), str.indexOf("&", str.indexOf("&caid=") + 1));
			} catch (Exception e) {
				caidValue = "NULL";
			}
			if (caidValue.equals("NULL")) {
				queryValues.put("caidValue", "NULL");
			} else {
				queryValues.put("caidValue", caidValue.split("=")[1]);
			}
			
			String csidValue;
			try {
				csidValue = str.substring(str.indexOf("&csid="), str.indexOf("&", str.indexOf("&csid=") + 1));
			} catch (Exception e) {
				csidValue = "NULL";
			}
			if (csidValue.equals("NULL")) {
				queryValues.put("csidValue", "NULL");
			} else {
				queryValues.put("csidValue", csidValue.split("=")[1]);
			}
			
			String tValueForRequest;
			try {
				tValueForRequest = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

			} catch (Exception e) {
				tValueForRequest = "NULL";
			}
			if (tValueForRequest.equals("NULL")) {
				queryValues.put("tValue", "NULL");
			} else {
				queryValues.put("tValue", tValueForRequest.split("=")[1]);
			}			
			queryMap.put(query, queryValues);
		}
	}
	return queryMap;
}
		
	static LinkedHashMap<String, Map> getQueryValues(Object parser123) {
		ArrayList<String> queryArray = new ArrayList<String>();
		LinkedHashMap<String, Map> queryMap = new LinkedHashMap<String, Map>();
		HashMap<String, String> queryValues;

		JSONArray compleateJSON_array = (JSONArray) parser123;
		
		
		for (Object t : compleateJSON_array) {
			JSONObject obj_forPath = (JSONObject) t;

			if (obj_forPath.get("path") != null && !obj_forPath.get("path").toString().contains("/ad/g/1")) {
				queryArray.add(obj_forPath.get("query").toString());
			}else if(obj_forPath.get("path") != null && obj_forPath.get("path").toString().contains("/ad/g/1")){
//				System.out.println(obj_forPath.get("path"));
				queryArray.add(stringCheck(obj_forPath.get("query").toString()));
			}
		}
		
		//to get cn ,adid , tvalue and tpos values
		int counter = 0;
		String query;
		for (String str : queryArray) {
			query = "query";
			queryValues = new HashMap<String, String>();
			counter = counter + 1;
			query = query + counter;
			str = stringCheck(str);

			if (str.contains("&cn=")) {
				String tValue;
				try {

					tValue = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

				} catch (Exception e) {
					tValue = "NULL";
				}
				if (tValue.equals("NULL")) {
					queryValues.put("tValue", "NULL");
				} else {
					queryValues.put("tValue", tValue.split("=")[1]);
				}
				String cnValue;
				try {
					cnValue = str.substring(str.indexOf("&cn="), str.indexOf("&", str.indexOf("&cn=") + 1));
				} catch (Exception e) {
					cnValue = "NULL";
				}
				if (cnValue.equals("NULL")) {
					queryValues.put("cnValue", "NULL");
				} else {
					queryValues.put("cnValue", cnValue.split("=")[1]);
				}
				String tPosValue;
				try {
					tPosValue = str.substring(str.indexOf("&tpos="), str.indexOf("&", str.indexOf("&tpos=") + 1));
				} catch (Exception e) {
					tPosValue = "NULL";
				}
				if (tPosValue.equals("NULL")) {
					queryValues.put("tPosValue", "NULL");
				} else {
					queryValues.put("tPosValue", tPosValue.split("=")[1]);
				}
				String adidValue;
				try {
					adidValue = str.substring(str.indexOf("&adid="), str.indexOf("&", str.indexOf("&adid=") + 1));
				} catch (Exception e) {
					adidValue = "NULL";
				}
				if (adidValue.equals("NULL")) {
					queryValues.put("adidValue", "NULL");
				} else {
					queryValues.put("adidValue", adidValue.split("=")[1]);
				} 
				if (!queryValues.get("cnValue").contains("videoView")) {
					if (!queryValues.get("cnValue").contains("concreteEvent")) {
						queryMap.put(query, queryValues);
					}
				}
			}else{
				
				String caidValue;
				try {
					caidValue = str.substring(str.indexOf("&caid="), str.indexOf("&", str.indexOf("&caid=") + 1));
				} catch (Exception e) {
					caidValue = "NULL";
				}
				if (caidValue.equals("NULL")) {
					queryValues.put("caidValue", "NULL");
				} else {
					queryValues.put("caidValue", caidValue.split("=")[1]);
				}
				
				String csidValue;
				try {
					csidValue = str.substring(str.indexOf("&csid="), str.indexOf("&", str.indexOf("&csid=") + 1));
				} catch (Exception e) {
					csidValue = "NULL";
				}
				if (csidValue.equals("NULL")) {
					queryValues.put("csidValue", "NULL");
				} else {
					queryValues.put("csidValue", csidValue.split("=")[1]);
				}
				
				String tValueForRequest;
				try {
					tValueForRequest = str.substring(str.indexOf("&t="), str.indexOf("&", str.indexOf("&t=") + 1));

				} catch (Exception e) {
					tValueForRequest = "NULL";
				}
				if (tValueForRequest.equals("NULL")) {
					queryValues.put("tValue", "NULL");
				} else {
					queryValues.put("tValue", tValueForRequest.split("=")[1]);
				}
				
				queryMap.put(query, queryValues);
			}
		

		}
		return queryMap;
	}

	static String getTVALUE(JSONParser parser, String newString,int val)
			throws ParseException {
		String tValue = null;
		try {
			Object obj_BodyText = parser.parse(newString);
			JSONArray text_array = (JSONArray) obj_BodyText;
			int Counter =0;
			
			top: for (Object t : text_array) {
				JSONObject ads_Obj = (JSONObject) t;
				
				JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
				JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
				for (Object ads : adSlotsArray) {
					JSONObject eventCalls = (JSONObject) ads;
					JSONArray forURL = (JSONArray) eventCalls.get("eventCallbacks");
					for (Object u : forURL) {
						JSONObject L = (JSONObject) u;
						
						try {

							tValue = L.get("url").toString().substring(L.get("url").toString().indexOf("&t="), L.get("url").toString().indexOf("&", L.get("url").toString().indexOf("&t=") + 1));
							 if(Counter == val){
								 return tValue.split("=")[1];
							 }
						} catch (Exception e) {
							tValue = "NULL";
						}

					}
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to parse the string: " +newString);
			e.printStackTrace();
		}
			return tValue.split("=")[1];
	}
		
	static LinkedHashMap<String, ArrayList<String>> getRollTypes(JSONParser parser, String newString)
			throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			// System.out.println(adSlotsArray.size());
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				String rollType = eventCalls.get("timePositionClass").toString();
				// System.out.println(rollType);

				if (rollType.equalsIgnoreCase("midroll")) {
					midcounter = midcounter + 1;
					addName = "midroll_" + midcounter;
				} else if (rollType.equalsIgnoreCase("preroll")) {
					precounter = precounter + 1;
					addName = "preroll_" + precounter;
				} else if (rollType.equalsIgnoreCase("postroll")) {
					precounter = precounter + 1;
					addName = "postroll_" + postcounter;
				}

				// JSONObject eventCalls = (JSONObject) ads;
				adIDs_Array = new ArrayList();
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");
				for (Object u : forURL) {

					JSONObject L = (JSONObject) u;
					// System.out.println(L.get("adId"));
					adIDs_Array.add(L.get("adId").toString());
				}
				map_rollsTypeAddids.put(addName, adIDs_Array);
			}
		}
		return map_rollsTypeAddids;
	}
		
	static LinkedHashMap<String, String> exp_timePositonValues(JSONParser parser, String newString)
			throws ParseException {
		
		LinkedHashMap<String, String> map_rolls_tPOS = new LinkedHashMap<String, String>();
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			// System.out.println(adSlotsArray.size());
			String addName = null;
			int midcounter = 0;
			int precounter = 0;
			int postcounter = 0;
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				String tPos = eventCalls.get("timePosition").toString();
				String rollType = eventCalls.get("timePositionClass").toString();
				

				if (rollType.equalsIgnoreCase("midroll")) {
					midcounter = midcounter + 1;
					addName = "midroll_" + midcounter;
				} else if (rollType.equalsIgnoreCase("preroll")) {
					precounter = precounter + 1;
					addName = "preroll_" + precounter;
				} else if (rollType.equalsIgnoreCase("postroll")) {
					precounter = precounter + 1;
					addName = "postroll_" + postcounter;
				}

				map_rolls_tPOS.put(addName, tPos);
			}
			
		}
		return map_rolls_tPOS;
	}

	static void getsAddIDs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			String str_rollName = null;
			for (Object ads : adSlotsArray) {
				
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");

				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					System.out.println(L.get("adId"));

				}

			}
		}
	}

	static void getsiteSection_eventCallbacksURLs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("eventCallbacks");
				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					System.out.println(L.get("url"));
				}
			}
		}
	}

	static void getsiteSection_selectedAdsURLs(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONObject adArrays = (JSONObject) ((JSONObject) ads_Obj.get("siteSection")).get("videoPlayer");
			JSONArray adSlotsArray = (JSONArray) ((JSONObject) adArrays.get("videoAsset")).get("adSlots");
			System.out.println(adSlotsArray.size());
			for (Object ads : adSlotsArray) {
				JSONObject eventCalls = (JSONObject) ads;
				JSONArray forURL = (JSONArray) eventCalls.get("selectedAds");
				for (Object u : forURL) {
					JSONObject L = (JSONObject) u;
					// System.out.println(L.get("url"));
					JSONArray eventCallsbacks = (JSONArray) L.get("eventCallbacks");
					for (Object ec : eventCallsbacks) {
						JSONObject urlSelectedAds = (JSONObject) ec;
						System.out.println(urlSelectedAds.get("url"));
					}
				}

			}
		}
	}

	static void getAds(JSONParser parser, String newString) throws ParseException {
		Object obj_BodyText = parser.parse(newString);
		JSONArray text_array = (JSONArray) obj_BodyText;
		for (Object t : text_array) {
			JSONObject ads_Obj = (JSONObject) t;
			JSONArray adArrays = (JSONArray) ((JSONObject) ads_Obj.get("ads")).get("ads");
			for (Object ads : adArrays) {
				JSONObject adID = (JSONObject) ads;
				System.out.println(adID.get("adId"));
			}
		}
	}

	public static String stringCheck(String data) {

		data = data.replace("%20", " ");
		data = data.replace("%21", "!");
		data = data.replace("%22", "\"");
		data = data.replace("%23", "#");
		data = data.replace("%24", "$");
		data = data.replace("%25", "%");
		data = data.replace("%26", "&");
		data = data.replace("%27", "\'");
		data = data.replace("%28", "(");
		data = data.replace("%29", ")");
		data = data.replace("%2A", "*");
		data = data.replace("%2B", "+");
		data = data.replace("%2C", ",");
		data = data.replace("%2D", "-");
		data = data.replace("%2E", ".");
		data = data.replace("%2F", "/");
		data = data.replace("%30", "0");
		data = data.replace("%31", "1");
		data = data.replace("%32", "2");
		data = data.replace("%33", "3");
		data = data.replace("%34", "4");
		data = data.replace("%35", "5");
		data = data.replace("%36", "6");
		data = data.replace("%37", "7");
		data = data.replace("%38", "8");
		data = data.replace("%39", "9");
		data = data.replace("%3A", ":");
		data = data.replace("%3B", ";");
		data = data.replace("%3C", "<");
		data = data.replace("%3D", "=");
		data = data.replace("%3E", ">");
		data = data.replace("%3F", "?");
		data = data.replace("%40", "@");
		data = data.replace("%41", "A");
		data = data.replace("%42", "B");
		data = data.replace("%43", "C");
		data = data.replace("%44", "D");
		data = data.replace("%45", "E");
		data = data.replace("%46", "F");
		data = data.replace("%47", "G");
		data = data.replace("%48", "H");
		data = data.replace("%49", "I");
		data = data.replace("%4A", "J");
		data = data.replace("%4B", "K");
		data = data.replace("%4C", "L");
		data = data.replace("%4D", "M");
		data = data.replace("%4E", "N");
		data = data.replace("%4F", "O");
		data = data.replace("%50", "P");
		data = data.replace("%51", "Q");
		data = data.replace("%52", "R");
		data = data.replace("%53", "S");
		data = data.replace("%54", "T");
		data = data.replace("%55", "U");
		data = data.replace("%56", "V");
		data = data.replace("%57", "W");
		data = data.replace("%58", "X");
		data = data.replace("%59", "Y");
		data = data.replace("%5A", "Z");
		data = data.replace("%5B", "[");
		data = data.replace("%5C", "\\");
		data = data.replace("%5D", "]");
		data = data.replace("%5E", "^");
		data = data.replace("%5F", "_");
		data = data.replace("%60", "`");
		data = data.replace("%61", "a");
		data = data.replace("%62", "b");
		data = data.replace("%63", "c");
		data = data.replace("%64", "d");
		data = data.replace("%65", "e");
		data = data.replace("%66", "f");
		data = data.replace("%67", "g");
		data = data.replace("%68", "h");
		data = data.replace("%69", "i");
		data = data.replace("%6A", "j");
		data = data.replace("%6B", "k");
		data = data.replace("%6C", "l");
		data = data.replace("%6D", "m");
		data = data.replace("%6E", "n");
		data = data.replace("%6F", "o");
		data = data.replace("%70", "p");
		data = data.replace("%71", "q");
		data = data.replace("%72", "r");
		data = data.replace("%73", "s");
		data = data.replace("%74", "t");
		data = data.replace("%75", "u");
		data = data.replace("%76", "v");
		data = data.replace("%77", "w");
		data = data.replace("%78", "x");
		data = data.replace("%79", "y");
		data = data.replace("%7A", "z");
		data = data.replace("%7B", "{");
		data = data.replace("%7C", "|");
		data = data.replace("%7D", "}");
		data = data.replace("%7E", "~");
		data = data.replace("%80", "`");
		data = data.replace("+", " ");
		return data;
	}
	
	public static String getFailReasons(String expe_value,String act_value){
		List<String> missing_List = new ArrayList<String>();
		String return_String=null;
		if(expe_value.length()==act_value.length()){
			List<String> exp_Items = Arrays.asList(expe_value.split("\\s*,\\s*"));
			List<String> act_Items =  Arrays.asList(act_value.split("\\s*,\\s*"));
			for(int i=0;i<exp_Items.size();i++){					
				if(!exp_Items.get(i).equals(act_Items.get(i))){
					missing_List.add(act_Items.get(i));
				}	
			}
			
			return_String = " - Fired AdBeacons order is wrong: "+missing_List;
		}else if(expe_value.length()<act_value.length()){
			List<String> exp_Items = Arrays.asList(expe_value.split("\\s*,\\s*"));
			List<String> act_Items =  Arrays.asList(act_value.split("\\s*,\\s*"));			
			act_Items.stream().filter( x -> !exp_Items.contains(x) ).forEach(x -> missing_List.add(x));
			return_String = " - Unexpected value got fired: "+missing_List;
		}else if(expe_value.length()>act_value.length()){
			List<String> exp_Items = Arrays.asList(expe_value.split("\\s*,\\s*"));
			List<String> act_Items =  Arrays.asList(act_value.split("\\s*,\\s*"));
			exp_Items.stream().filter( x -> !act_Items.contains(x) ).forEach(x -> missing_List.add(x));				
			return_String = " - AdBeacons are missing: "+missing_List;
		}
		
		
		return return_String;
		
	}
		
	public static String getSlotImpression(LinkedHashMap<String, Map> qu, String tvalue, int i, String tops) {
		List<String> indexes = new ArrayList<String>(qu.keySet());
		int counter = 0;
		String returnStrin = "SlotImpression is not present: FAIL";
		for (int j = 0; j < qu.size(); j++) {
			if (!qu.get(indexes.get(j)).toString().toLowerCase().contains("caid"))
				if (qu.get(indexes.get(j)).toString().split(",")[3].split("=")[1].replace("}", "").equals(tops)
						&& qu.get(indexes.get(j)).toString().contains(tvalue)) {
					if (qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{", "").replace("]", "")
							.toLowerCase().equals("SlotImpression".toLowerCase())
							&& qu.get(indexes.get(j)).toString().contains(tvalue)) {
						counter = counter + 1;
						returnStrin = "SlotImpression is present: PASS";
						break;
					}
				}
		}
		return returnStrin;
	}
	
	public static String getSlotImpression(LinkedHashMap<String, Map> qu, String tvalue, int i) {
		List<String> indexes = new ArrayList<String>(qu.keySet());
		LinkedHashSet<String> tops_Set = new LinkedHashSet<>();
		LinkedHashSet<String> tValue_Set = new LinkedHashSet<>();
		int counter = 0;
		String returnStrin = "SlotImpression is not present: FAIL";
		for (int j = 0; j < qu.size(); j++) {
			if (!qu.get(indexes.get(j)).toString().toLowerCase().contains("caid"))
				if (qu.get(indexes.get(j)).toString().contains(tvalue)) {
					if (qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{", "").replace("]", "")
							.toLowerCase().equals("SlotImpression".toLowerCase())
							&& qu.get(indexes.get(j)).toString().contains(tvalue)) {
						counter = counter + 1;
						returnStrin = "SlotImpression is present: PASS";
						break;
					}
				}
		}
		return returnStrin;
	}
		
	public static String getSlotEnd(LinkedHashMap<String, Map> qu, String tvalue, int i, String tops) {
		List<String> indexes = new ArrayList<String>(qu.keySet());
		int counter = 0;
		String returnStrin = "SlotEnd is not present: FAIL";
		for (int j = 0; j < qu.size(); j++) {
			if (!qu.get(indexes.get(j)).toString().toLowerCase().contains("caid"))
				if (qu.get(indexes.get(j)).toString().split(",")[3].split("=")[1].replace("}", "").equals(tops)
						&& qu.get(indexes.get(j)).toString().contains(tvalue)) {
					if (qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{", "").replace("]", "")
							.toLowerCase().equals("Slotend".toLowerCase())
							&& qu.get(indexes.get(j)).toString().contains(tvalue)) {
						counter = counter + 1;
						returnStrin = "SlotEnd is present: PASS";
						break;
					}
				}
		}
		return returnStrin;
	}
		
	public static String getSlotEnd(LinkedHashMap<String, Map> qu, String tvalue, int i) {
		List<String> indexes = new ArrayList<String>(qu.keySet());
		int counter = 0;
		String returnStrin = "SlotEnd is not present: FAIL";
		for (int j = 0; j < qu.size(); j++) {
			if (!qu.get(indexes.get(j)).toString().toLowerCase().contains("caid"))
				if (qu.get(indexes.get(j)).toString().contains(tvalue)) {
					if (qu.get(indexes.get(j)).toString().split(",")[0].split("=")[1].replace("{", "").replace("]", "")
							.toLowerCase().equals("Slotend".toLowerCase())
							&& qu.get(indexes.get(j)).toString().contains(tvalue)) {
						counter = counter + 1;
						returnStrin = "SlotEnd is present: PASS";
						break;
					}
				}
		}
		return returnStrin;
	}

}